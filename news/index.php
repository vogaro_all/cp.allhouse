<?php
require_once(dirname(__FILE__)."/../lacne/news/output/post.php");

$page_limit = 15;

if (isset($_GET['category']) && $_GET['category'] && is_numeric($_GET['category'])) {
    $category = $_GET['category'];
    $url_param = 'category='.$category;
}

$params = array(
    "category" => $category,
    "page_limit" => $page_limit,
);
$datas = LACNE_PostList($params);

$date_archive =  get_archives();
?>
<?php include_once('./index_template.html');?>