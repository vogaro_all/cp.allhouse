$(document).ready(function() {

    $(".menu-icon").click(function() {
        $('.header-contact').toggle();
        $(".gnav-sp").slideToggle("slow");
        $(this).toggleClass('is-active');
        return false;
    });

    $('.gnav-sp li ul').hide();
    $(".gnav-sp ul li").click(function() {
        $("ul", this).slideToggle('slow');
    });

    $("li").click(function(event) {
        event.stopPropagation();
    });

    $(window).resize(function() {
        var windowWidth = $(window).outerWidth();

        if (windowWidth <= 768 &&
            $('.menu-icon').hasClass('is-active')) {
            $('.gnav-sp').show();
        } else if (windowWidth > 768 &&
            $(".gnav-sp").is(":visible")) {
            $('.gnav-sp').hide();
        }

    });

    $(".service-nav").hide();
    $("#close").click(function() {
        $(".service-nav").hide();
    });
    $("#show").click(function() {
        $(".service-nav").show();
    });

    // header menu active with body ID
    if ($("body").attr("id") == "strength") {
        $("#strength-menu a").addClass("active");
    }
    if ($("body").attr("id") == "service") {
        $("#service-menu a").addClass("active");
    }
    if ($("body").attr("id") == "gaiyo") {
        $("#gaiyo-menu a").addClass("active");
    }
    if ($("body").attr("id") == "news") {
        $("#news-menu a").addClass("active");
    }
    if ($("body").attr("id") == "sdgs") {
        $("#sdgs-menu a").addClass("active");
    }
    if ($("body").attr("id") == "recruit") {
        $("#recruit-menu a").addClass("active");
    }

    // header service menu dropdown
    $("#service-menu").on("mouseenter", function() {
        $(this).find(".mega-menu-content").addClass("show")
    }).on("mouseleave", function() {
        $(this).find(".mega-menu-content").removeClass("show")
    });

});