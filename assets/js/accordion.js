$(document).ready(function(){
	$(".item-01").click(function(){
		$(".row1").toggle();
		$(".service-block").not(".row1").css("display", "none");
		$(".service-item").not(".item-01").removeClass("current");

		if ($(".row1").is(":visible")) {
			$(".item-01").addClass("current");
			$(".service-block").addClass("fade-in");

		} else {
			$(".item-01").removeClass("current"); 
		}
	});

	$(".item-02").click(function(){
		$(".row2").toggle();
		$(".service-block").not(".row2").css("display", "none");
		$(".service-item").not(".item-02").removeClass("current");

		if ($(".row2").is(":visible")) {
			$(".item-02").addClass("current"); 
			$(".service-block").addClass("fade-in");

		} else {
			$(".item-02").removeClass("current"); 
		}
	});

	$(".item-03").click(function(){
		$(".row3").toggle();
		$(".service-block").not(".row3").css("display", "none");
		$(".service-item").not(".item-03").removeClass("current");

		if ($(".row3").is(":visible")) {
			$(".service-block").addClass("fade-in");

			$(".item-03").addClass("current"); 
		} else {
			$(".item-03").removeClass("current"); 
		}
	});

	$(".item-04").click(function(){
		$(".row4").toggle();
		$(".service-block").not(".row4").css("display", "none");
		$(".service-item").not(".item-04").removeClass("current");

		if ($(".row4").is(":visible")) {
			$(".service-block").addClass("fade-in");

			$(".item-04").addClass("current"); 
		} else {
			$(".item-04").removeClass("current"); 
		}
	});

	$(".item-05").click(function(){
		$(".row5").toggle();
		$(".service-block").not(".row5").css("display", "none");
		$(".service-item").not(".item-05").removeClass("current");

		if ($(".row5").is(":visible")) {
			$(".service-block").addClass("fade-in");

			$(".item-05").addClass("current"); 
		} else {
			$(".item-05").removeClass("current"); 
		}
	});

	$(".item-06").click(function(){
		$(".row6").toggle();
		$(".service-block").not(".row6").css("display", "none");
		$(".service-item").not(".item-06").removeClass("current");

		if ($(".row6").is(":visible")) {
			$(".service-block").addClass("fade-in");

			$(".item-06").addClass("current"); 
		} else {
			$(".item-06").removeClass("current"); 
		}
	});

});