$(document).ready(function() {

    // sec-site //
    $('ul.tabs li').click(function() {
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        })

    // footer-accordion //
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        //   Variables privadas
        var links = this.el.find('.link');
        //   Evento
        links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.acd-child').not($next).slideUp().parent().removeClass('open');
        };
    }

    var accordion = new Accordion($('.acd-parent'), false);

    // page-top //
    $(document).ready(function() {
    
        var topBtn = $('#page-top');
        topBtn.hide();
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) { 
                topBtn.fadeIn();
            } else {
                topBtn.fadeOut();
            }
    });

    topBtn.click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
});
    
     if((navigator.userAgent.match(/iPad/i))) {
        $('#gnav .mega-menu').on('click',function(event){
             event.preventDefault();
            $("#gnav .mega-menu-content").css({ "display": "block" });
            $(this).toggleClass('.mega-menu-content');
        })      
    }


// external スムーズスクロール

$(function() {
    setTimeout(function() {
        if (location.hash) {
            window.scrollTo(0, 0);
            target = location.hash.split('#');
            smoothScrollTo($('#' + target[1]));
        }
    }, 1);

    function smoothScrollTo(target) {
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top + 10 }, 1000);
        }
    }
});



});


