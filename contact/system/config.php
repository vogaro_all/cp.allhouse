<?php
ini_set('error_reporting', 0);

require_once(dirname(__FILE__).'/lib/common.php');
require_once(dirname(__FILE__).'/lib/validation.php');
require_once(dirname(__FILE__).'/lib/sendmail.php');
require_once(dirname(__FILE__).'/lib/CsrfValidator.php');

//テンプレート
define("FORM_INPUT", dirname(__FILE__)."/../index_template.html");
define("FORM_CONFIRM", dirname(__FILE__)."/../confirm_template.html");

//define("ADMIN_MAIL_ADDRESS", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_1", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_2", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_3", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_4", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_5", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_6", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_7", "okada@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS_8", "okada@vogaro.co.jp");

define("ADMIN_MAIL_ADDRESS", "allhouseform@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_1", "baibai@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_2", "info@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_3", "pm-info@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_4", "kenchiku@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_5", "reform@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_6", "info@milifeplus.co.jp");
define("ADMIN_MAIL_ADDRESS_7", "allhouseform@allhouse.co.jp");
define("ADMIN_MAIL_ADDRESS_8", "pr-allhouse@allhouse.co.jp");

// メール送信フラグ
define("SEND_FLG", 1);

//バリデーションクラスインスタンス生成
$VALIDATION = new validation();

//メールクラスインスタンス生成
$MAIL = new sendmail();

$CSRvalidator = new CsrfValidator();

$_CATEGORY = array(
    1=>'不動産購入・売却',
    2=>'不動産賃貸',
    3=>'賃貸管理',
    4=>'注文住宅',
    5=>'リフォーム・リノベーション',
    6=>'保険・相続',
    7=>'その他',
    8=>'広報・取材',
    
);