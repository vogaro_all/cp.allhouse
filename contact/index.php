<?php
session_start();

require_once(dirname(__FILE__).'/system/config.php');

//完了画面へ遷移
if(!empty($_POST['comp'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);
	
	// CSRF対策
	if (!CsrfValidator::validate(filter_input(INPUT_POST, 'token'))) {
	    include_once(FORM_INPUT);
		exit;
	}
	
	// フォーム部分作成
	$contents = "お問い合わせ種別：".$_CATEGORY[$data_list['category']]."\r\n";
	$contents .= "お名前：".$data_list['name']."\r\n";
	$contents .= "ふりがな：".$data_list['kana']."\r\n";
	$contents .= "電話番号：".$data_list['tel']."\r\n";
	$contents .= "E-Mail：".$data_list['email']."\r\n";
	$contents .= "お問い合せ日時：".date('Y/m/d H:i')."\r\n";
	$contents .= "お問い合わせ内容：\r\n";
	$contents .= $data_list['body']."\r\n";
	
	
	$subject = "※要対応※【お問い合わせ】".$data_list['name']."様へ連絡をお願いします。";
	
	$body = "下記のお客様よりお問い合わせをいただきました。\r\n";
	$body .= "\r\n";
	$body .= "━━━━━━お問い合わせ内容━━━━━━"."\r\n";
	$body .= $contents;
	$body .= "━━━━━━━━━━━━━━━━━━━━━━━━━\r\n";
	$body .= "\r\n";
	$body .= "ご対応をお願いいたします。"."\r\n";
	$body .= ""."\r\n";
	$body .= "本メールは、オールハウス株式会社にお問い合わせがあったことを通知する目的で送信しています。"."\r\n";
	$body .= ""."\r\n";
	
	$admin_mail = '';
	$department = '';
	if ($data_list['category'] == 1) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_1.','.ADMIN_MAIL_ADDRESS;
	    $department =
'－－－－－－－－－－－－
オールハウス株式会社　売買事業部
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-236-3596
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 2) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_2.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社　賃貸事業部
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-236-8948
FAX：082-546-9077
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 3) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_3.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社　プロパティマネジメント事業部
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-236-7771
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 4) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_4.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社　建築事業部
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-236-3597
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－';
	} elseif ($data_list['category'] == 5) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_5.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社　リフォーム事業部
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-236-3598
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 6) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_6.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-890-1002
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 7) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_7;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-890-1002
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	} elseif ($data_list['category'] == 8) {
	    $admin_mail = ADMIN_MAIL_ADDRESS_8.','.ADMIN_MAIL_ADDRESS;
	    $department = '－－－－－－－－－－－－
オールハウス株式会社
郵便番号：〒735-0012　
住所：広島県安芸郡府中町八幡1-4-23
電話番号：082-890-1002
FAX：082-890-1003
営業時間：平日9時～18時
－－－－－－－－－－－－
';
	}
	
	if (SEND_FLG) {
		//管理者宛メール
	    $MAIL->send($admin_mail, $subject, $body, 'お問い合わせ <'.ADMIN_MAIL_ADDRESS.'>');
	}
//	var_dump($admin_mail);
//	var_dump($body);
	
	$subject = "【オールハウス株式会社】お問い合わせありがとうございます。";
	
	$body = $data_list['name']." 様"."\r\n";
	$body .= ""."\r\n";
	$body .= "この度はオールハウス株式会社へのお問い合わせありがとうございました。"."\r\n";
	$body .= ""."\r\n";
	$body .= "以下の内容でお問い合わせを受け付けいたしました。"."\r\n";
	$body .= "3営業日以内に、担当者よりご連絡いたしますので"."\r\n";
	$body .= "今しばらくお待ちくださいませ。"."\r\n";
	$body .= "何卒よろしくお願い申し上げます。"."\r\n";
	$body .= ""."\r\n";
	$body .= "━━━━━━お問い合わせ内容━━━━━━"."\r\n";
	$body .= $contents;
	$body .= "━━━━━━━━━━━━━━━━━━━━━"."\r\n";
	$body .= $department;
	$body .= "\r\n";
	$body .= "※このメールはシステムからの自動返信となります。"."\r\n";
	$body .= "　送信専用アドレスのため、このメールにはご返信頂けませんので"."\r\n";
	$body .= "　ご注意くださいませ。"."\r\n";
	$body .= "-----------------------------"."\r\n";
	$body .= ""."\r\n";
	
	if (SEND_FLG) {
		//問い合わせ者宛メール
	    $MAIL->send($data_list['email'], $subject, $body,  'お問い合わせ <'.ADMIN_MAIL_ADDRESS.'>');
	}
//	var_dump($body);
//	exit;
	header("Location: ./complete.html");
}

//確認画面へ遷移
else if(!empty($_POST['conf'])){

	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	// CSRF対策
	if (!CsrfValidator::validate(filter_input(INPUT_POST, 'token'))) {
	    include_once(FORM_INPUT);
		exit;
	}
	//入力チェック
	$err_check_arr = array(
	    "category"=>array("name"=>"お問い合わせ種別","type"=>array("null")),
	    "name"=>array("name"=>"お名前","type"=>array("null","len"),"length"=>200),
	    "kana"=>array("name"=>"ふりがな","type"=>array("len"),"length"=>200),
	    "tel"=>array("name"=>"電話番号","type"=>array("phone")),
	    "email"=>array("name"=>"メールアドレス","type"=>array("null","len","email"),"length"=>255),
		"body"=>array("name"=>"お問い合わせ内容","type"=>array("null", "len"),"length"=>2000),
	    "agree"=>array("name"=>"同意","type"=>array("null")),
		);
	
	//バリデーション実行
	$err = $VALIDATION->check($data_list,$err_check_arr);

	if (isset($err['agree']) && !empty($err['agree'])) {
	    $err['agree'] = 'メールフォームでのお問い合わせにはプライバシーポリシーに同意頂く必要があります。';
	}
	
	if(empty($err)){
	    include_once(FORM_CONFIRM);
	}else{
	    include_once(FORM_INPUT);
	}
}

//入力画面へ戻る
else if(!empty($_POST['back'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);
	include_once(FORM_INPUT);
}

//入力画面
else{
	// デフォルト設定
    include_once(FORM_INPUT);
}
