<?php
use lacne\core\model\SocialConnect;
/**
 *-------------------------------------------------------------------------
 *  プラグイン： Twitterつぶやき,facebook投稿機能
 *  social_connect.php
 *  php ver >= 5.1
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_SOCIAL_CONNECT" , 1);

require_once(dirname(__FILE__)."/twitteroauth.php");
require_once(dirname(__FILE__)."/facebook-php-sdk/src/facebook.php");

if(!defined("TABLE_OPTION_SOCIAL")) define("TABLE_OPTION_SOCIAL" , "social_connect");

class Lib_social_connect extends Lib_setting
{
    
    /**
     *  コンストラクタ
     * 
     *  @param  $db DBオブジェクト
     *  @return void
     */
    function Lib_social_connect($LACNE , $db){
        parent::Lib_setting($LACNE , $db);
    }
    
    function get_key()
    {
    }
    
    
    function setting_html()
    {
    }
    
    /**
     * 認証情報をDBから取得して返す
     * @return object
     */
    function get_connect_info()
    {
        
        $connect_info = array();
        $rs = with(new SocialConnect())->get_infodata();
        if($rs && is_array($rs))
        {
            //DBから取得したtwitter or facebookの認証用データを展開して格納
            foreach($rs as $val)
            {
                if(isset($val["type"]) && $val["type"])
                {
                    $connect_info[$val["type"]] = $val;
                }
            }
        }
        
        return $connect_info;
    }
    
    /**
     * 認証を解除する処理
     * 
     * @param string $type (twitter or facebook)
     * @return boolean
     */
    function auth_off($type)
    {
        $updata = array(
            "type" => $type,
            "account" => "",
            "token1" => "",
            "token2" => "",
            "modified" => fn_get_date()
        );
        
        return with(new SocialConnect())->replace($updata , "type");
        
    }
    
    /**
     * Twitter投稿
     * @param string $message
     * @return void 
     */
    function twitter_post($message){
        
        $connect_info = $this->get_connect_info();
        if(isset($connect_info["twitter"]) && $connect_info["twitter"])
        {
            $twitterOauth = new TwitterOAuth($connect_info["twitter"]["key1"],$connect_info["twitter"]["key2"] ,$connect_info["twitter"]["token1"],$connect_info["twitter"]["token2"]);
            $ret = $twitterOauth->OAuthRequest("https://api.twitter.com/1/statuses/update.xml","POST",array("status"=>$message));
        }
        
        return;
    }
    
    /**
     * Facebook投稿
     * @param string $message
     * @return void 
     */
    function facebook_post($message){
        
        $connect_info = $this->get_connect_info();
        if(isset($connect_info["facebook"]) && $connect_info["facebook"])
        {
            $facebook = new Facebook(array(
                'appId' => $connect_info["facebook"]["key1"],    // App ID/API
                'secret' => $connect_info["facebook"]["key2"],    // アプリの秘訣
                'cookie' => true,
            ));
            
            $data = array( 'access_token' => $connect_info["facebook"]["token1"],
              'message' => $message
            );
            $ret = $facebook->api("/".$connect_info["facebook"]["account"]."/feed", 'post', $data);

        }
        
        return;
    }
}

