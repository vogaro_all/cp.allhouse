<?php

/**
 * 管理画面メニュー設定
 *
 */

$_SETTING_ADMIN_MENU = array(

    //Menu 2
    "news" => array(
        "menu_id"	=> "Extend01",
        "label"     => "お知らせ・EVENT管理",
        "link"		=> LACNE_PATH."/news/admin/index.php",
        "icon"      => "news",
        "comment"	=> "お知らせ・EVENTの情報を管理・編集することができます",
        "submenu"   =>array(
        //            //Menu 2_1
        //            array(
            //                "label" => "インデックス",
        //                "link"  => LACNE_PATH."/news/admin/index.php",
        //                "icon"  => "main",
        //				"comment" => "",
        //            ),
            //Menu 2_2
            array(
                "label" => "記事一覧",
                "link"  => LACNE_PATH."/news/admin/article/index.php",
                "icon"  => "list",
                "comment" => "登録されている%sの一覧を確認できます。",
            ),
            //Menu 2_3
            array(
                "label" => "記事作成",
                "link"  => LACNE_PATH."/news/admin/article/edit.php",
                "icon"  => "edit",
                "comment" => "%sの作成、編集を行います。",
            ),
            //Menu 2_4
            //            array(
                //                "terms" => "manage_category",
            //                "label" => "カテゴリ管理",
            //                "link"  => LACNE_PATH."/news/admin/category/index.php",
            //                "icon"  => "category",
            //				"comment" => "カテゴリの新規登録、削除を行います。",
            //            ),
                //Menu 2_5
            array(
                "label" => "メディアアップロード",
                "link"  => LACNE_PATH."/news/admin/media/index.php",
                "icon"  => "media",
                "comment" => "%s内に掲載するメディアファイルのアップロード、削除を行います。",
            ),
        )
        ),
);


/*
 * 以下、indexページと連動させる用の
 * 大きめのアイコン画像設定
 */

$_SETTING_INDEXMENU_IMAGES = array(
	"list" => "main_menu_img01.gif",
	"news" => "main_menu_img02.gif",
	"category" => "main_menu_img03.gif",
	"account" => "main_menu_img04.gif",
	"media" => "main_menu_img05.gif",
);
