<?php

/**
 *  mobileHtmlConvert.php(PHP5用をPHP4にも対応させた）
 *  携帯用html変換処理
 *  @package    Lacne
 *  @access     public
 */ 


class mobileHtmlConvert {
    
    var $params;
    
    function mobileHtmlConvert(){
    }
    
    function singleton($text,$env='docomo',$dflg=true) {
        $this->params['dflg'] = ($env==='docomo' and $dflg===true) ? true:false;
            return preg_replace_callback(
                '/<([^>\s]+)(?:\s+([^>]+?))?>/i',
                array($this,'_rep_tags'),
                preg_replace_callback(
                    '/<\/([a-zA-Z0-9]+)>/',
                array($this,'_rep_end_tags'),$text) );
    }
    function _rep_tags($matches) {
        $tag    = &$matches[1];
        $option    = &$matches[2];
        switch($tag) {
            case '?xml':
                return '<?xml'.$this->put($option).">\n";
            case '!DOCTYPE':
                return '<!DOCTYPE'.$this->put($option).">\n";
            case 'meta':
                return '<meta '.( ($this->params['dflg'] === true)
                    ?str_replace(array('text/html;'),array('application/xhtml+xml;'),$option)
                    : $option )." />\n";
            case 'br':
                return '<br'.( ($this->params['dflg'] === true)
                    ? ' /'
                    : ( ($option) ? $this->put($option) : '' ) ).'>';
            case 'blink':
                return ( ($this->params['dflg'] === true)
                    ? '<div style="text-decoration:blink">'
                    : ( ($option)
                        ? '<blink'.$this->put($option).'>'
                        : '<blink>' ) );
            case 'div':
                return '<div'.( ($option) ? $this->_convert($option,'block') : '' ).'>';
            case 'h1':
            case 'h2':
            case 'h3':
            case 'h4':
            case 'h5':
            case 'h6':
                return '<'.$tag.( ($option) ? $this->_convert($option,'block') : '' ).'>';
            case 'font':
                return ( ($option)
                    ? $this->_convert($option,'inline')
                    : ( ($this->params['dflg'] === true)
                        ? '<span>'
                        : '<font>' ) );
                break;
            case 'hr':
                return '<hr'.( ($option) ? $this->_convert($option,'hr')
                    : ( ($this->params['dflg'] === true)
                        ? ' /'
                        : '' ) ).'>';
				/*
            case 'a':
                return '<a'.( ($option) ? $this->_link_convert($option,'link'):'').'>';
				 * 
				 */
            case 'ul':
                return '<ul'.( ($option) ? $this->_convert($option,'list'):'').'>';
            case 'li':
                return '<li'.( ($option) ? $this->_convert($option,'list'):'').'>';
            /*
			case 'textarea':
                return ($this->params['dflg'] === true )
                    ? '<textarea'.$this->put_input($option)." />\n"
                    : '<textarea'.$this->put($option)." />\n";
			 * 
			 */
            case 'p':
                return '<p'.( ($option) ? $this->_convert($option,'block') : '' ).'>';
            case 'table':
                return '<table'.( ($option) ? $this->put($option) : '' ).'>';
            case 'img':
                return '<img '.$option.' />';
            case 'marquee':
                return ( ($this->params['dflg'] === true)
                        ? '<div'.( ($option)? $this->_convert($option,'marquee'):'').'>'
                        : '<marquee'.$this->_convert($option,'marquee').'>' );
        }
        return '<'.$tag.( ($option) ? $this->_convert($option,'other'):'').'>';
    }
    function _rep_end_tags($matches) {
        $tag    = &$matches[1];
        switch($tag) {
            case 'font':
                return ( ($this->params['dflg'] === true)
                        ? '</span>'
                        : '</font>' );
            case 'blink':
                return ( ($this->params['dflg'] === true)
                        ? '</div>'
                        : '</blink>' );
            case 'marquee':
                return ( ($this->params['dflg'] === true)
                        ? '</div>'
                        : '</marquee>' );
        }
        return '</'.$tag.'>';
    }
    function _convert($option,$type="block") {
        switch($type) {
            case 'block':
            case 'hr':
            case 'list':
                return ( ($this->params['dflg'] === true)
                    ? ' style="'.$this->compiler_option($option,$type).'"'
                    : ' '.trim($this->compiler_option($option,$type)));
 
            case 'marquee':
                return ( ($this->params['dflg'] === true)
                    ? ' style="display:-wap-marquee;'.$this->compiler_option($option,"marquee").'"'
                    : ' '.trim($this->compiler_option($option,"marquee")) );
 
            case 'inline':
                return ( ($this->params['dflg'] === true)
                    ? '<span style="'.$this->compiler_option($option,'inline').'">'
                    : '<font '.trim($this->compiler_option($option,'inline')).'>');
 
            case 'link':
            case 'other':
            case 'meta':
                return ' '.trim($this->compiler_option($option,$type));
        }
    }
    function compiler_option($string,$type='block') {
        $list    = explode(' ',$string);
        $ret    = '';
        $func    = 'compiler_option_'.$type;
        foreach($list As $l) {
            list($element,$param) = explode('=',$l,2);
            //$ret .= $this->func($element,str_replace('\\',' ',str_replace(array('"',"'"),"",$param)));
            $ret .= call_user_func_array(array($this,$func),array($element,str_replace('\\',' ',str_replace(array('"',"'"),"",$param))));
        }
        return $ret;
    }
    function compiler_option_block($element,$param) {
        switch($element) {
            case 'align':
                return ( ($this->params['dflg'] === true)
                    ? 'text-align:'.$param.';'
                    : 'align="'.$param.'" ' );
            case 'bgcolor':
                return ( ($this->params['dflg'] === true)
                    ? 'background-color:'.$param.';'
                    : 'bgcolor="'.$param.'" ' );
            case 'style':
                return ( ($this->params['dflg'] === true)
                    ? (preg_match('/;$/',$param)?$param:$param.';')
                    : 'style="'.$param.'" ' );
        }
    }
    function compiler_option_table($element,$param) {
        switch($element) {
            case 'align':
                return ( ($this->params['dflg'] === true)
                    ? 'text-align:'.$param.';'
                    : 'align="'.$param.'" ' );
            case 'bgcolor':
                return ( ($this->params['dflg'] === true)
                    ? 'background-color:'.$param.';'
                    : 'bgcolor="'.$param.'" ' );
            case 'style':
                return ( ($this->params['dflg'] === true)
                    ? $param
                    : 'style="'.$param.'" ' );
        }
    }
    function compiler_option_marquee($element,$param) {
        switch($element) {
            case 'behavior':
                return ( ($this->params['dflg'] === true)
                    ? '-wap-marquee-style:'.$param.';'
                    : 'behavior="'.$param.'" ' );
            case 'direction':
                return ( ($this->params['dflg'] === true)
                    ? '-wap-marquee-dir:'.(($param==='right')?'ltr':'rtl').';'
                    : 'direction="'.$param.'" ' );
            case 'loop':
                $param    = (($param=='-1' or $params=='0')?'infinite':$param);
                return ( ($this->params['dflg'] === true)
                    ? '--wap-marquee-loop:'.$param.';'
                    : 'loop="'.$param.'" ' );
            case 'bgcolor':
                return ( ($this->params['dflg'] === true)
                    ? 'background-color:'.$param.';'
                    : 'bgcolor="'.$param.'" ' );
            case 'style':
                return ( ($this->params['dflg'] === true)
                    ? $param
                    : 'style="'.$param.'" ' );
        }
    }
    function compiler_option_inline($element,$param) {
        switch($element) {
            case 'color':
                return ( ($this->params['dflg'] === true)
                    ? 'color:'.$param.';'
                    : 'color="'.$param.'" ' );
            case 'size':
                return ( ($this->params['dflg'] === true)
                    ? 'font-size:'.$this->_mini_font_size_xml($param).';'
                    : 'size="'.$param.'" ' );
            case 'style':
                return ( ($this->params['dflg'] === true)
                    ? $param
                    : 'style="'.$param.'" ' );
        }
    }
    function compiler_option_hr($element,$param) {
        switch($element) {
            case 'align':
                return ( ($this->params['dflg'] === true)
                    ? 'float:'.$param.';'
                    : 'align="'.$param.'" ' );
            case 'color':
                return ( ($this->params['dflg'] === true)
                    ? 'background-color:'.$param.';'
                    : 'color="'.$param.'" ' );
            case 'size':
                return ( ($this->params['dflg'] === true)
                    ? 'height:'.$param.';'
                    : 'size="'.$param.'" ' );
            case 'width':
                return ( ($this->params['dflg'] === true)
                    ? 'width:'.$param.';'
                    : 'width="'.$param.'" ' );
            case 'style':
                return ( ($this->params['dflg'] === true)
                    ? $param
                    : 'style="'.$param.'" ' );
            case 'noshade':
                return ( ($this->params['dflg'] === true)
                    ? ( ($param==='on') ? 'border-style:solid;': '')
                    : ( ($param==='on') ? 'noshade': '') );
        }
    }
    function compiler_option_link($element,$param) {
        switch($element) {
            case 'name':
                return ( ($this->params['dflg'] === true)
                    ? 'id="'.$param.'" '
                    : 'name="'.$param.'" ' );
        }
        return $element.'="'.$param.'" ';
    }
    function compiler_option_list($element,$param) {
        switch($element) {
            case 'type':
                return ( ($this->params['dflg'] === true)
                    ? 'list-style-type:'.$this->_mini_list_style($param).';'
                    : 'type="'.$param.'" ' );
        }
    }
    function compiler_option_other($element,$param) {
        return $element.'="'.$param.'" ';
    }
    function _mini_font_size_xml($int) {
        switch($int) {
            case 1:
                return 'xx-small';
            case 2:
                return 'small';
            case 4:
                return 'large';
            case 5:
                return 'large';
            case 6:
                return 'x-large';
            case 7:
                return 'xx-large';
            default:
                return 'medium';
        }
    }
    function _mini_list_style($string) {
        switch($string) {
            case '1':
                return 'decimal';
            case 'A':
                return 'upper-alpha';
            case 'a':
                return 'lower-alpha';
            default:
                return $string;
        }
    }
    function put($option) {
        return ' '.$option;
    }
}

?>