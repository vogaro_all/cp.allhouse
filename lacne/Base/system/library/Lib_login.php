<?php
use lacne\core\Library;
use lacne\core\model\Admin;
/**
// ------------------------------------------------------------------------
 * lib_login.php
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_login extends Library
{
    /**
     * Lib_login constructor.
     * @param $LACNE
     * @param $db
     */
	function Lib_login($LACNE , $db) {
        parent::__construct($LACNE, $db);
	}
	
        /**
	 *  ログインチェック
         *  @param string $login_account
         *  @param string $password
         *  @param boolean $set_session //認証成功後、ログイン情報をセッションセットするか
         * 
         *  @return boolean
	 */	
	function checkLogin($login_account,$password,$set_session = true) {
           
            //管理者アカウントデータを取得
            $rs_data = with(new Admin())->fetchOneByLoginAccount($login_account);
            
            if($rs_data && isset($rs_data["password"]))
            {
                // ユーザ入力パスワードの判定
                if (sha1(CERT_LOGIN_KEY.$password) == $rs_data["password"]) {
                    
                    //パスワード判定OK
                    //次にIPアドレス判定
                    $ip_check = $this->check_allow_ipaddress($login_account);

                    if($ip_check) {
                        if($set_session)
                        {
                            // セッション登録
                            $this->setLoginSession($rs_data['login_id'],CERT_STRING,$rs_data["authority"]);
                        }
                        return true;
                    }
                }
            }
            
            return false;

	}
    
    /**
     *  接続元IPアドレス制限の判定
     * 
     *  @param string  $login_account
     *  @return boolean
     */
    function check_allow_ipaddress($login_account) {
        //管理者アカウントデータを取得
        $rs_data = with(new Admin())->fetchOneByLoginAccount($login_account);
        
        if(!empty($rs_data["ipaddress"])) {
            $this_ip = fn_getIP();
            $allow_ip_arr = explode("," , $rs_data["ipaddress"]);
            if(!in_array($this_ip , $allow_ip_arr)) {
                return false;
            }
        }

        return true;
    }

	/**
	 *  認証セッションへセット
	 * 
	 *  @param string  $login_id
         *  @param string  $cert_string
         *  @param string  $authority
	 *  @return boolean
	 */
	function setLoginSession($login_id,$str,$authority="MASTER") {
	
        $this->LACNE->session->regenerate();
		$_SESSION["login_id"] = $login_id;
		$_SESSION["ip"] = fn_getIP();
		$_SESSION["cert"] = $str;
                $_SESSION["authority"] = $authority;
		
                return TRUE;
	}
        
        /**
	 *  認証成功の判定
	 *  @param boolean $redirect (認証エラーの場合そのままリダイレクトするか)
	 *  @param string $controller_name (accountオプションで利用)
	 *  @return boolean
	 */
	function IsSuccess($redirect = true , $controller_name = "") {
		
            if(isset($_SESSION["cert"]) && isset($_SESSION["login_id"]))
            {
		//CERT_STRING,login_id,IPの照合
		if($_SESSION["cert"] == CERT_STRING && $_SESSION["login_id"] && $_SESSION["ip"] == fn_getIP()) {
                    
                    return $_SESSION["login_id"];
		}
            }	
	    
            if($redirect)
            {
                fn_redirect(LINK_LOGIN_ERR01);
            }
            else
            {
                return false;
            }
	}
		
	/** セッションの破棄 */
	function EndSession() {
		// デフォルトは、「PHPSESSID」
		$sname = session_name();
		// セッション変数を全て解除する
		$_SESSION = array();
		// セッションを切断するにはセッションクッキーも削除する。
		// Note: セッション情報だけでなくセッションを破壊する。
		if (isset($_COOKIE[$sname])) {
			setcookie($sname, '', time()-42000, '/');
		}
		// 最終的に、セッションを破壊する
		session_destroy();
	}
	
	/** 関連セッションのみ破棄する。 */
	function logout() {
		unset($_SESSION['cert']);
		unset($_SESSION['login_id']);
		unset($_SESSION['ip']);
		unset($_SESSION['authority']);
                
                $this->EndSession();
                
                return;
	}
        
        /** 
         * アカウント情報一覧を取得する（アカウント管理オプションが有効の場合のみ利用可能）
         * 無効の場合はindexページに強制遷移
         * @param number $num 
         */
        function get_AccountList($num=0) {
            //アカウント管理オプション(option/account/account.php)で本機能を実装
            //オプションが無効の場合は indexページにリダイレクト
            fn_redirect(LACNE_APP_ADMIN_PATH."/index.php");
        }
        
        /**
	 *  login_accountとemailからアカウント存在チェックし
         *  存在すればパスワードを再発行する
         *  @param string $login_account
         *  @param string $email
         * 
         *  @return boolean
	 */	
	function passRemind($login_account,$email) {
           
            //check
            $tid = with(new Admin())->checkAccount($login_account , $email);
            
            if($tid)
            {
                //仮パスワードを発行
                $pass0 = substr(uniq() , 0 , 8);
                $pass = sha1(CERT_LOGIN_KEY.$pass0);

                $updata = array(
                    "id" => $tid,
                    "password" => $pass,
					"modified" => fn_get_date()
                );

                $result = with(new Admin())->replace($updata , "id");
                
                if($result)
                {
                    //メール本文作成
                    $mail_message = array(
                        "login_account" => $login_account,
                        "password" => $pass0
                    );
                    $mail_body = $this->LACNE->render("mail_forget_message" , $mail_message , true);

                    //メール送信
                    $from = FROM_MAINADDRESS;
                    $this->LACNE->load_library(array('sendmail'));
                    $this->LACNE->library["sendmail"]->sendmail($email , "パスワード再発行のお知らせ" , $mail_body , $from);

                    return true;
                }
                
            }
            
            return false;
	}
        
        
        
        /**
         * 操作権限があるかをチェック(承認フローオプションで利用）
         * @param string $action
         * @param string $login_id //指定しなければSESSIONから取得
         * @return boolean
         */
        public function chk_controll_limit($action , $login_id = "")
        {
            return 1;
        }

        /**
         * 操作対象の記事データが自身が作成した記事かどうかチェック（承認フローオプションで利用）
         * @param $post_id
         * @param string $login_id 指定しなければSESSIONから取得
         * @return bool
         */
        public function chk_my_edit_post($post_id , $login_id = "")
        {
            return true;
        }
        
        /**
         * セッションIDを返す
         * @return string
         */
        public function get_session_id()
        {
            return session_id();
        }
        
}

?>