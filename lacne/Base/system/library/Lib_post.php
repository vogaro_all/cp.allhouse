<?php
use lacne\core\Library;
use lacne\core\model\Post;
use lacne\core\model\PostMeta;
use lacne\core\model\Category;
use lacne\core\model\Pref;
use lacne\core\model\City;

/**
// ------------------------------------------------------------------------
 * Lib_post.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_post extends Library
{
        /**  @var string  $authority  権限タイプ */
        var $authority;

    /**
     * Lib_post constructor.
     * @param $LACNE
     * @param $db
     */
	function Lib_post($LACNE , $db) {
        parent::__construct($LACNE, $db);
	}
	
        
    /**
     * 指定された絞り込み条件をURLに付加させるパラメータに変換
     * @param object $search_param
     * @return string
     */
    function get_searchlink_param($search_param)
    {
        $searchlink = "";

        if(is_array($search_param) && count($search_param))
        {
            foreach($search_param as $key => $val)
            {
                if($searchlink) $searchlink .= "&";
                $searchlink .= $key."=".$val;
            }
        }

        return $searchlink;
    }
        
        /**
         * 記事の公開切り替え
         * 
         * @param number $id 
         */
        function change_publish($id , $login_id)
        {
            if(is_numeric($id) && with(new Post())->cnt("id = ?" , array($id)))
            {
                $rs = with(new Post())->fetchOne($id);
                if($rs)
                {
                    $output_flag = $rs["output_flag"];
                    
                    $updata = array();
                    
                    if($output_flag == 1){
                        $updata["output_flag"] = 0;
                    }else{
                        $updata["output_flag"] = 1;
                    }
                    
                    $updata["id"] = $id;
                    $tid = with(new Post())->replace($updata , "id");
                    
		}
            }
            return;
        }
        
        /**
         * 一覧中の「公開 / 非公開」メニュー表示
         * @param object $post_data
         * @param boolean $smp (スマフォの場合は画像なし)
         */
        function publish_menu($post_data , $smp = false)
        {
            $return = "";
            if(isset($post_data["output_flag"]))
            {
                $return .= '<span class="link_publish" id="publish_'.$post_data["id"].'">';
                if($post_data["output_flag"] == 1)
                {
                    if(!$smp)
                    {
                        $return .= '<img src="'.LACNE_SHAREDATA_PATH.'/images/common/table_img_public_on.gif" alt="公開" />';
                    }
                    else
                    {
                        $return .= '公開';
                    }
                }
                else
                {
                    if(!$smp)
                    {
                        $return .= '<img src="'.LACNE_SHAREDATA_PATH.'/images/common/table_img_public_off.gif" alt="未公開" />';
                    }
                    else
                    {
                        $return .= '未公開';
                    }
                
                }
                
                $return .= '</span>';
            }
            
            return $return;
        }
        
        
        /**
         * 指定したPostIDのデータを取得（metaデータも含めて）
         * @param number $post_id 
         */
        function get_postdata($post_id)
        {
            
            $post_data = with(new Post())->fetchOne($post_id);
            $meta_data = with(new PostMeta())->fetchByPostID($post_id);
            
            if($meta_data && is_array($meta_data))
            {
                foreach($meta_data as $meta)
                {
                    $post_data["_meta_"][$meta["meta_key"]] = $meta["meta_body"];
                }
            }
            
            return $post_data;
        }
        
        /**
         * カテゴリ選択プルダウン用のカテゴリ登録リストを取得
         * 
         * @return object
         */
        function get_category_list()
        {
            $result = with(new Category())->fetchAll();
            
            $category_data = array();
            if($result && count($result))
            {
                foreach($result as $value)
                {
                    $category_data[$value["id"]] = $value["category_name"];
                }
            }
            return $category_data;
        }
        
        
        /**
         * 記事データを保存
         * @param object $data //保存する記事データ
         * @param string $key  //key
         * @param string $login_id
         */
        function replace_post($data , $key , $login_id="")
        {
            $tid = with(new Post())->replace($data , $key);
            return $tid;
        }
        
        
        /**
         * 指定したPostIDとメタフィールド名をKEYとしてメタデータを登録する
         * @param number $post_id
         * @param object $data_list 
         */
        function replace_meta($post_id , $meta_data)
        {
            $data_list = array();
            if($meta_data && is_array($meta_data))
            {
                foreach($meta_data as $key => $meta)
                {
                    $data_list = array(
                        "post_id" => $post_id,
                        "meta_key" => $key,
                        "meta_body" => $meta,
                        "modified" => fn_get_date()
                    );
                    
                    with(new PostMeta())->replace($data_list , array("post_id","meta_key"));
                }
            }
            return;
        }
        
        /**
         * 表示日付を一覧表示用に整形して返す
         * @param string $output_date
         * @return string 
         */
        function view_output_date($output_date)
        {
            //表示日付が未来の日付であれば、日付表示を時間まで表示させ 
            //公開予約状態にあることをわかるようにする
            if(strtotime($output_date) > strtotime('now'))
            {
                return '<strong>'.fn_dateFormat($output_date, "Y年m月d日").'<br />'.fn_dateFormat($output_date, "H時i分").' に公開</strong>';
            }
            
            return fn_dateFormat($output_date);
        }
        
        /**
         * 記事一覧で表示させるデータ件数表示のhtml
         * @param number $cnt
         * @param number $page
         * @param number $page_num
         * @param number $output_num
         * @return string 
         */
        function html_post_list_pager_head($cnt , $page , $page_num , $output_num)
        {
            $str_num1 = (!$cnt)?0:($page-1)*$output_num+1;
            $str_num2 = ($page == $page_num) ? $cnt : $page*$output_num;
            return $cnt."件中 <strong>".$str_num1."-".$str_num2."件</strong>表示中";
        }
       
}

?>