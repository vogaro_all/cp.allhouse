<?php
use lacne\core\Library;
/**
// ------------------------------------------------------------------------
 * Lib_output.php
 * フロント側表示処理まわりの操作などいろいろやる
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

include_once(DIR_VENDORS."rhaco/Rhaco.php");
Rhaco::import("tag.model.SimpleTag");

//画像リサイズ用ライブラリ読み込み
if(file_exists(DIR_VENDORS."class.imgresize.php"))
{
    require_once(DIR_VENDORS."class.imgresize.php");
}
class Lib_output extends Library
{

    /**
     * Lib_output constructor.
     * @param $LACNE
     * @param $db
     */
    function Lib_output($LACNE , $db) {
        parent::__construct($LACNE, $db);
    }

    /**
     * imgタグにその画像をモーダル表示させるためのclass属性割り当ての処理を行う
     * @param string $html
     * @return string 
     */
    function set_modallink_for_img($html0)
    {
        $html = fn_output_html_txtarea($html0);
        $html = "<tag>".$html."</tag>";

        if(SimpleTag::setof($tag , $html , "tag"))
        {
            $img_tags = $tag->f("in(img)");
            
            if(is_array($img_tags) && count($img_tags))
            {
                for($i=0;$i<count($img_tags);$i++)
                {
                    //モーダル表示させるためのclass属性割り当て
                    if(defined('USE_SETTING_LIGHTBOX') && USE_SETTING_LIGHTBOX)
                    {
                        $tag->f("img[{$i}].saveParam(class)",USE_SETTING_LIGHTBOX);
                    }
                }
                $return = $tag->get();
                $return = str_replace('<tag>','',$return);
                $return = str_replace('</tag>','',$return);
                return $return;
            }
        }

        return $html0;

    }

    /**
     * imgタグのリンクをhttpから始まるURL形式に変換する
     * @param string $html
     * @return string 
     */
    function set_http_path($html0)
    {
        $html = fn_output_html_txtarea($html0);
        $html = "<tag>".$html."</tag>";
        
        if(SimpleTag::setof($tag , $html , "tag"))
        {
            $img_tags = $tag->f("in(img)");
            
            if(is_array($img_tags) && count($img_tags))
            {
                for($i=0;$i<count($img_tags);$i++)
                {
                    //リンクをhttpから始まるURL形式に変換する
                    $src = $tag->f("img[{$i}].param(src)");
                    if(substr($src,0,4) != "http")
                    {
                        $tag->f("img[{$i}].saveParam(src)",$this->LACNE->get_urlpath($src));
                    }

                }
                $return = $tag->get();
                $return = str_replace('<tag>','',$return);
                $return = str_replace('</tag>','',$return);
                return $return;
            }
        }

        return $html0;
    }
    
    /**
     * htmlタグ中のimaを抽出し、その中のsrc属性値のみを返す（social_btnのOGPセットで利用）
     * @param string $html
     * @return array 
     */
    function get_imagesrc($html0)
    {
        $html = fn_output_html_txtarea($html0);
        $html = "<tag>".$html."</tag>";
        $imgsrc = array();
        
        if(SimpleTag::setof($tag , $html , "tag"))
        {
            $img_tags = $tag->f("in(img)");
            
            if(is_array($img_tags) && count($img_tags))
            {
                for($i=0;$i<count($img_tags);$i++)
                {
                    $imgsrc[] = $tag->f("img[{$i}].param(src)");
                }
            }
        }

        return $imgsrc;
    }
    
    
    /**
     * （ckEditor利用時）flvファイルの埋め込みがある場合、objectタグを生成
     * [[movie_|_filepath_|_width_|_height]]で書き出されたテキストをobjectタグに変換するかたち
     * また paramタグのname="src"値は書き出し時に"FileName"に書き換える IE系で表示されなくなるため
     * @param string $html
     * @return string
     */
    function set_flv_objct_param($html0)
    {
        
        //[[movie_|_filepath_|_width_|_height]] を抽出
        preg_match_all("/(\[\[movie_\|_[\w\d\/_\.\+\-]+_\|_[0-9]+_\|_[0-9]+\]\])/u"  , $html0 , $match);
        
        if(count($match) && isset($match[0]) && count($match[0]))
        {
            foreach($match[0] as $flv_str)
            {
                $t_str1 = explode("_|_" , $flv_str);
                $flv_filepath = (isset($t_str1[1]) && $t_str1[1])?$t_str1[1]:"";
                $flv_size_w = (isset($t_str1[2]) && $t_str1[2])?$t_str1[2]:"500";
                $flv_size_h = (isset($t_str1[3]) && $t_str1[3])?$t_str1[3]:"500";
                
                $replace_str = "";
                if($flv_filepath)
                {
                    $replace_str = '<object type="application/x-shockwave-flash" data="'.LACNE_SHAREDATA_PATH.'/js/flv/player_flv_maxi.swf"';
                    if($flv_size_w)
                    {
                        $replace_str .= ' width="'.$flv_size_w.'"';
                    }
                    if($flv_size_h)
                    {
                        $replace_str .= ' height="'.$flv_size_h.'"';
                    }
                    $replace_str .= '>';
                    $replace_str .= '<param name="FileName" value="'.$flv_filepath.'">';
                    $replace_str .= '<param name="movie" value="'.LACNE_SHAREDATA_PATH.'/js/flv/player_flv_maxi.swf">';
                    $replace_str .= '<param name="allowFullScreen" value="true">';
                    $replace_str .= '<param name="FlashVars" value="flv='.$flv_filepath.'&amp;showstop=1&amp;showvolume=1&amp;showtime=1">';
                    $replace_str .= '<embed src="/dev_lacne/news/upload/data1.flv" type="application/x-shockwave-flash" quality="high"';
                    if($flv_size_w)
                    {
                        $replace_str .= ' width="'.$flv_size_w.'"';
                    }
                    if($flv_size_h)
                    {
                        $replace_str .= ' height="'.$flv_size_h.'"';
                    }
                    $replace_str .= '>';
                    $replace_str .= '</object>';
                    
                    $html0 = str_replace($flv_str, $replace_str , $html0);
                }
            }
        }
        
        return $html0;
    }
    
    
    /**
     * （elrteエディタ利用時）flvファイルの埋め込みがある場合、objectタグのclassid , codebase属性を消す
     * また paramタグのname="src"値は書き出し時に"FileName"に書き換える IE系で表示されなくなるため
     * @param string $html
     * @return string
     */
    function delete_flv_objct_param($html0)
    {
        $html = fn_output_html_txtarea($html0);
        $html = "<tag>".$html."</tag>";
        
        if(SimpleTag::setof($tag , $html , "tag"))
        {
            $object_tags = $tag->f("in(object)");
            
            if(is_array($object_tags) && count($object_tags))
            {
                for($i=0;$i<count($object_tags);$i++)
                {
                    //type値がapplication/x-shockwave-flashかどうか
                    $type = $tag->f("object[{$i}].param(type)");
                    if($type == "application/x-shockwave-flash")
                    {
                        $tag->f("object[{$i}].saveParam(classid)","");
                        $tag->f("object[{$i}].saveParam(codebase)","");
                    }

                }
            }
            
            $param_tag = $tag->f("in(param)");
            if(is_array($param_tag) && count($param_tag))
            {
                for($i=0;$i<count($param_tag);$i++)
                {
                    //name値がsrcかどうか
                    $name = $tag->f("param[{$i}].param(name)");
                    if($name == "src")
                    {
                        $tag->f("param[{$i}].saveParam(name)","FileName");
                    }
                }
            }
            
            $return = $tag->get();
            $return = str_replace('<tag>','',$return);
            $return = str_replace('</tag>','',$return);
            return $return;
        }

        return $html0;
    }
    
    
    /*
     * デバイス判別（本機能はmobile output拡張オプションで実装）
     * @param void
    */
    function is_mobile () {
        return "";
    }
	
	
    /*
     * 画像を指定サイズでリサイズする処理
     * (すでにリサイズした同じ画像が存在すればそれを利用する）
     * @param string $device (smph" or "mobi")
     * @param string $imgsrc //画像パス
     * @param int $resize_w //リサイズ幅サイズ
     * @param int $resize_h //リサイズ縦サイズ
     * @return mixed
     */
    function _img_resize($device , $imgsrc , $resize_w = "" , $resize_h = "")
    {
        
		$resize_width = !empty($resize_w) ? $resize_w : 0;
		$resize_height = !empty($resize_h) ? $resize_h : 0;
		
        if($resize_width || $resize_height)
        {
            $img_path = $this->LACNE->pathChange_dir_root($imgsrc);
            
            if(file_exists($img_path))
            {
            
                $resizeImg = new Imgresize($img_path);
                
				//指定したサイズより大きければリサイズする
				if(($resize_width > 0 && $resizeImg->image_width > $resize_width) || ($resize_height > 0 && $resizeImg->image_height > $resize_height))
				{
					$resize_name = $resizeImg->name()."_".$device;
					
					if(!file_exists($resizeImg->dir."/".$resize_name.".".$resizeImg->ext))
					{
						$resizeImg->name($resize_name);
						if($resize_width)
						{
							$resizeImg->width($resize_width);
						}
						if($resize_height)
						{
							$resizeImg->height($resize_height);
						}
						$resizeImg->save();

					}
					
					return $this->LACNE->get_path_docroot($resizeImg->dir)."/".$resize_name.".".$resizeImg->ext;
				}
				else
				{
					return $imgsrc;
				}
            }
        }
        return $imgsrc;
    }
}

?>