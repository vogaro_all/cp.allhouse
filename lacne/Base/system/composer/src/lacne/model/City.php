<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_city.php
 * カテゴリデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class City extends Model
{


    /**
     *  コンストラクタ
     *
     *  @param  $session Sessionオブジェクト
     *  @return void
     */
    function __construct(){ parent::__construct(); }

	function fetchAll()
	{
		$sql = "SELECT * FROM ". TABLE_CITY ." ORDER BY id ASC";
		return $this->_fetchAll($sql, array());
	}

	function fetchOne($id)
	{
		if(is_numeric($id))
		{
			$sql = "SELECT * FROM ". TABLE_CITY ." WHERE id = ?";
			return $this->_fetchOne($sql, array($id));
		}

		return;
	}

	function getCityList($id) {

		$sql = "SELECT * FROM ".TABLE_CITY. " WHERE pref_id = " . $id  .  " ORDER BY id";
		return $this->_fetchAll($sql, array());
	}
}

?>