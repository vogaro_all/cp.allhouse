<?php namespace lacne\core\model;
use lacne\core\Model;
/**
// ------------------------------------------------------------------------
 * model_postmeta.php
 * 記事メタデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class PostMeta extends Model
{
    /**
     *  コンストラクタ
     *
     *  @param  $session Sessionオブジェクト
     *  @return void
     */
    function __construct(){ parent::__construct(); }

    /**
     *
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POSTMETA);
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($where , $param)
    {

        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POSTMETA)." WHERE ".$where;
        return $this->_fetchOne($sql, $param);

    }

    /**
     * 指定したpost_idのメタデータ取得
     * @param string $post_id
     * @return object
     */
    function fetchByPostID($post_id)
    {

        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POSTMETA)." WHERE post_id = ?";
        return $this->_fetchAll($sql, array($post_id));

    }

    /**
     *
     * データのインサートもしくはアップデート処理
     *
     * @param string $table
     * @param object $data
     * @param string $key
     * @param boolean $auto_quote
     */
    function replace($data , $key , $auto_quote = true)
    {
        $tid = $this->adodb_replace($this->getTableName(TABLE_POSTMETA) , $data , $key);

        return $tid;
    }

    function delete($post_id)
    {
        if(is_numeric($post_id))
        {
            $sql = "DELETE FROM ".$this->getTableName(TABLE_POSTMETA)." WHERE post_id = ?";
            $this->_execute($sql, array($post_id));
        }

        return;
    }


    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_POSTMETA) , $where , $param);
    }

    /**
     * post.idを一括置換する
     * @param number $target_post_id
     * @param number $change_post_id
     */
    function change_post_id($target_post_id , $change_post_id)
    {
        $sql = "UPDATE ".$this->getTableName(TABLE_POSTMETA)." SET post_id = ? WHERE post_id = ?";

        return $this->_execute($sql, array($change_post_id , $target_post_id));

    }

}

?>