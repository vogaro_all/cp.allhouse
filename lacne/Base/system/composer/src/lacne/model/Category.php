<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_category.php
 * カテゴリデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Category extends Model
{

    /**
     * Category constructor.
     */
    public function __construct(){ parent::__construct(); }

    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_CATEGORY)." ORDER BY id ASC";
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".$this->getTableName(TABLE_CATEGORY)." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    function replace($data , $key)
    {
        return $this->adodb_replace($this->getTableName(TABLE_CATEGORY) , $data , $key);
    }

    function delete($id)
    {

        if(is_numeric($id))
        {

            $sql = "DELETE FROM ".$this->getTableName(TABLE_CATEGORY)." WHERE id = ?";
            return $this->_execute($sql, array($id));

        }

        return;
    }

    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_CATEGORY) , $where , $param);
    }

}

?>