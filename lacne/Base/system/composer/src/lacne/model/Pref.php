<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_pref.php
 * カテゴリデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Pref extends Model
{


	    /**
	     * Category constructor.
	     */
	    public function __construct(){ parent::__construct(); }

        function fetchAll()
        {
            $sql = "SELECT * FROM ". TABLE_PREF ." ORDER BY id ASC";
            return $this->_fetchAll($sql, array());
        }

        function fetchOne($id)
        {
            if(is_numeric($id))
            {
                $sql = "SELECT * FROM ". TABLE_PREF ." WHERE id = ?";
                return $this->_fetchOne($sql, array($id));
            }

            return;
        }
}

?>