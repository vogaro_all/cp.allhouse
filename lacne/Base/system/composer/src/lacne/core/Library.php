<?php namespace lacne\core;
/**
 * Class Library
 * @package lacne\core
 */
class Library
{

    /**
     * @var DBオブジェクト
     */
    protected $db;

    /**
     * @var $LACNE
     */
    protected $LACNE;

    /**
     * Library constructor.
     * @param $LACNE
     * @param $db
     */
    public function __construct($LACNE,$db)
    {
        $this->LACNE = $LACNE;
        $this->db = $db;
    }

}
