<?php

/**
 *-------------------------------------------------------------------------
 *
 *  共通ライブラリ
 *
 *-------------------------------------------------------------------------
 */

/**
 *  リダイレクト
 *
 *  @param  string $url 移動先URL
 *  @param  bool $is301
 *  @return void
 */
function fn_redirect( $url, $is301 = FALSE )
{
    if( $is301 )
    {
        header( "HTTP/1.1 301 Moved Permanently" );
    }
    header( "Location: " . $url );
    exit();
}


// シリアライズしつつ base64 エンコードをする関数
if ( ! function_exists( 'serialize_base64_encode' ) ) {
	function serialize_base64_encode($array) {
		$data = serialize($array);
		$data = base64_encode($data);
		return $data;
	}
}

// base64 デコードしつつシリアライズされたデータを復元する関数
if ( ! function_exists( 'unserialize_base64_decode' ) ) {
	function unserialize_base64_decode($data) {
		$data = base64_decode($data);
		$array = unserialize($data);
		return $array;
	}
}

/**
 *  日付取得
 *
 *  @param  void
 *  @return string
 */
function fn_get_date(){
    return Date("Y-m-d H:i:s");
}

/**
 *  年月日表示変換
 *
 *  @param  string $date
 *  @return string
 */
function fn_dateFormat($date,$format = "Y年m月d日") {
    return date($format,strtotime($date));
}

/**
 *  年月日時間表示変換2
 *
 *  @param  string $date
 *  @return string
 */
function fn_dateTimeFormat($datetime,$format = "Y年m月d日 H:i:s") {
    return date($format,strtotime($datetime));
}

/**
 * 日付の比較(日付が対象とする日付より先かどうか）
 *
 * @param datetime $date1
 * @param datetime $date2 //対象とする日付
 */
function fn_dateComparison($date1,$date2){

    if(strtotime($date1) > strtotime($date2)){
        return true;
    }else{
        return false;
    }
}

/**
 *  現在のページをセット
 *
 *  @param  int $pg  ページ値
 *  @return int
 */
function fn_now_page_set($pg){

    if(ctype_digit(strval($pg))){
        $page = $pg;
        if($page < 1){
                $page = 1;
        }
    }else{
        $page = 1;
    }

    return $page;
}

/**
 *  ページ数を計算
 *
 *  @param  int $cnt  総ページ数
 *  @param  int $num  1ページあたりの表示数
 *  @return int
 */
function fn_getPages($cnt,$num=10){

    if(is_numeric($cnt) && is_numeric($num))
    {
        if($cnt == 0){

            return 0;

        }else{
        	if($num === 0) return 0;
            $page_num = floor(($cnt-1)/$num) + 1;

            return $page_num;
        }
    }

    return 0;
}


/**
 *  ページング出力
 *
 *  @param  int $page  現在のページ数
 *  @param  int $page_num  総ページ数
 *  @param  boolean $return
 *  @return void
 */
function fn_pagenavi($page,$page_num,$param="",$return=false){

	$prev_page_txt = "前へ";
	$next_page_txt = "次へ";

	$show_nav = 9;
	//総ページの半分
	$show_navh = floor($show_nav / 2);
	//現在のページをナビゲーションの中心にする
	$loop_start = $page - $show_navh;
	$loop_end = $page + $show_navh;
	//現在のページが両端だったら端にくるようにする
	if ($loop_start <= 0) {
		$loop_start  = 1;
		$loop_end = $show_nav;
	}
	if ($loop_end > $page_num) {
		$loop_start  = $page_num - $show_nav +1;
		$loop_end =  $page_num;
	}

	if($param){
		$linkparam = '?'.$param.'&page=';
	}else{
		$linkparam = '?page=';
	}

	if($page_num > 1){

		$output_txt = '';

		$output_txt .= '<ul>';

		if($page > 1){
			$output_txt .= '<li class="prev"><a href="'.$linkparam.($page-1).'">'.$prev_page_txt.'</a></li>';
		}

		if($loop_start > 1) {
			$output_txt .= '<li class="count"><a href="'.$linkparam.'1">1</a></li>';
			if($loop_start > 2) {
				$output_txt .= '<li class="count cut"><span>...</span></li>';
			}
		}

		for ($i=$loop_start; $i<=$loop_end; $i++) {
			if ($i > 0 && $page_num >= $i) {
				if($i == $page){
					$output_txt .= "<li class=\"count active\"><span>".$i."</span></li>";
				}else{
					$output_txt .= "<li class=\"count\"><a href=\"".$linkparam.$i."\">".$i."</a></li>";
				}
			}
		}

		if($loop_end < $page_num) {
			if($loop_end < ($page_num - 1)) {
				$output_txt .= '<li class="count cut"><span>...</span></li>';
			}
			$output_txt .= '<li class="count"><a href="'.$linkparam.$page_num.'">'.$page_num.'</a></li>';
		}


		if($page < $page_num){
			$output_txt .= '<li class="next"><a href="'.$linkparam.($page+1).'">'.$next_page_txt.'</a></li>';
		}

		$output_txt .= '</ul>';

        if($return)
        {
            return $output_txt;
        }

		echo $output_txt;
	}

	return "";
}


/**
 *  年月日表示変換
 *
 *  @param  string $date
 *  @return string
 */
function fn_date_format02($date){

	return str_replace("-","/",$date);

}

/**
 *  POST送信されたフォームデータ取得
 *
 *  @param  array $data  送信されてきたフォーム値
 *  @return array
 */
function fn_get_form_param($data){

	//取得
	if(count($data)){
		foreach($data as $key=>$value){
                        //送信ボタン系のパラメータは除外
			if($key != "comp" && $key != "back" && $key != "conf" && $key != "charcode" && $key != "conf_x" && $key != "conf_y" && $key != "comp_x" && $key != "comp_y"){

				$data_list[$key] = $value;
			}
		}
		return $data_list;
	}
}

/**
 * 指定したデータ内の指定したKEYの値をURLパラメータ形式に変換
 * @param array $vars
 * @param array $keys
 * @param boolean $top_str (返却する文字列の最初に？を付けるかどうか)
 * @return string
 */
function fn_set_urlparam($vars , $keys , $top_str=true)
{
    $param = "";
    if(is_array($keys) && is_array($vars))
    {
        foreach($keys as $key)
        {
            if(isset($vars[$key]))
            {
                if(!$param)
                {
                    if($top_str)
                    {
                        $param .= "?";
                    }
                }
                else
                {
                    $param .= "&";
                }
                $param .= $key."=".fn_sanitize($vars[$key]);
            }
        }
    }

    return $param;
}


/**
 *  プルダウンメニュー生成(管理画面内で使用)
 *
 *  @param  array $list  選択させるデータ配列
 *  @param  int $selected  選択番号
 *  @return void
 */
function fn_output_html_select($list,$selected=0){

	if(count($list)){
		foreach($list as $key => $value){
			if($selected != ''){
				if($key == $selected){
					echo "<option value='$key' selected='selected'>$value</option>";
				}else{
					echo "<option value='$key'>$value</option>";
				}
			}else{
				echo "<option value='$key'>$value</option>";
			}

		}
	}

}

/**
 *  市区町村用プルダウンメニュー生成(管理画面内で使用)
 *
 *  @param  array $list  選択させるデータ配列
 *  @param  int $selected  選択番号
 *  @return void
 */
function fn_output_html_city_select($list,$selected=0){
    if(count($list)){
        foreach($list as $key => $value){
            if($selected != ''){
                if($value['id'] == $selected){
                    echo '<option value=' . $value['id'] . ' selected="selected">' . $value['city_name'] . '</option>';
                }else{
                    echo '<option value=' . $value['id'] . '>' . $value['city_name'] . '</option>';
                }
            }else{
                echo '<option value=' . $value['id'] . '>' . $value['city_name'] . '</option>';
            }
            
        }
    }
    
}

/**
 *  hidden値生成
 *
 *  @param  array $list  hidden配置するデータ
 *  @return void
 */
function fn_output_html_hidden($list){

	if(count($list)){
		foreach($list as $key => $value){
			if(is_array($value)){
				foreach($value as $key2 => $value2){
					if(is_array($value2)){
						foreach($value2 as $key3 => $value3){
							echo "<input type=\"hidden\" name=\"".$key."[".$key2."][".$key3."]\" value=\"".$value3."\" />\n";
						}
					}else{
						echo "<input type=\"hidden\" name=\"".$key."[".$key2."]\" value=\"".$value2."\" />\n";
					}
				}
			}else{

				if($key != "comp" && $key != "back" && $key != "conf" && $key != "conf_x" && $key != "conf_y" && $key != "conm_x" && $key != "conm_y"){
					echo "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
				}

			}
		}
		echo "<input type=\"hidden\" name=\"charcode\" value=\"文字コード認識用の文字列\" />";
	}
}

/**
 *  フォームのvalue値を生成
 *
 *  @param  mixed $value  データ
 *  @return void
 */
function fn_setvalue($val){
}

/**
 *  配列内の指定したKEYの値を開放
 *
 *  @param  array $arr  対象のデータ
 *  @param  string $unset_key  開放対象のキー
 *  @return array
 */
function fn_unset_form_param($arr,$unset_key){

	//取得
	if(count($arr)&&count($unset_key)){
		foreach($unset_key as $value){
			if(isset($arr[$value])){
				unset($arr[$value]);
			}
		}
		return $arr;
	}
}


/**
 *  数値型のチェック
 *
 *  @param  int $data
 *  @return bool
 */
function fn_check_int($data){

	if(is_numeric($data)){
		return true;
	}else{
		return false;
	}
}


/**
 *  エスケープ処理
 *
 *  @param  mixed $text エスケープするデータ
 *  @return mixed
 */
function fn_sanitize($text){

	if (is_array($text)) {
		return array_map('fn_sanitize', $text);
	}
	return htmlspecialchars($text,ENT_QUOTES);
}
/**
 * fn_sanitizeのエイリアス
 * @param mixed $text
 * @return mixed
 */
function fn_esc($text)
{
    return fn_sanitize($text);
}

/**
 * Javascriptコードを除去する
 * @param string $text
 * @return string
 */
function fn_strip_jscode($text)
{
    $text = preg_replace('/( )(on|On|oN|ON)(.{3,16}\=)/','__\2\3',$text);
    $text = preg_replace('/<(script)/','__\1',$text);
    $text = preg_replace('/(alert)(.{3,16}\=)/','__\1',$text);
    $text = preg_replace('/(location)(.{3,16}\=)/','__\1',$text);

    return $text;
}


/**
 *  ファイル拡張子の前に文字列をくっつける
 *
 *  @param  string $filename  ファイル名
 *  @return string
 */
function fn_filename_attach($filename,$str){

	$file_str["str"] = substr($filename,0,-4);
	$file_str["opt"] = substr($filename,-4);

	return $file_str["str"].$str.$file_str["opt"];

}

/**
 *  入力エラー内容出力
 *
 *  @param  array $err  エラー内容
 *  @return void
 */
function fn_input_err($err){

	echo "<span class='errtxt'>";
	if(count($err)){
		foreach($err as $value){
			echo $value."<br />";
		}
	}
	echo "</span>";
}


/**
 *  改行調整
 *
 *  @param  string $txt
 *  @return string
 */
function fn_change_wbr($string){

	$change_txt = explode(" ",$string);

	if(is_array($change_txt)){
		$r_txt = "";
		foreach($change_txt as $value){
			$r_txt .= $value."<wbr> ";
		}

		return $r_txt;
	}
	return false;
}


/**
 *  制限文字数に合わせて切り取り
 *
 *  @param  string $string
 *  @param  int $length
 *  @return string
 */
function fn_limit_string($string,$length){

	if(mb_strlen($string,STRINGCODE_PHP) > $length){
		$string = mb_substr($string,0,$length,STRINGCODE_PHP);
		$string .= ".....";
	}
	return $string;
}

/**
 *  2バイト文字チェック
 *
 *  @param  string $string
 *  @return string
 */
function fn_check_2byte($string){

	if(preg_match("/^[!-~\n\r ]+$/", $string)){
		return true;
	}else{
		return false;
	}
}

/**
 *  2バイト文字を数値参照化
 *
 *  @param  string $string
 *  @return string
 */
function fn_string_change_numeric($string){

	$map = array( 0, 0x10FFFF, 0, 0xFFFFFF );
	$str = mb_encode_numericentity( $string, $map, "UTF-8" );
	return $str;
}

/**
 *  改行コードを<br />に変換
 *
 *  @param  string $txt
 *  @return string
 */
function fn_output_html_txtarea($txt){

	$txt2 = str_replace("\r\n", "\r", $txt);	//Win系は\r\n
	$txt2 = str_replace("\r", "\n", $txt2);		//Mac系は\r
	$txt2 = str_replace("\n", "<br />", $txt2);	//\n除去して<br>

	return $txt2;
}

/**
 *  エラー文を出力（エラー内容を格納した配列を渡す必要がある）
 *
 *  @param  array $err
 *  @param  string $errtxt class name
 *  @return string
 */
function fn_output_errtxt($err,$headtxt = "",$headclass = ""){

    $errtxt = "";

    if(!is_array($err)) $err_arr = array($err);
    else $err_arr = $err;
	if(!empty($err_arr)){

            $class = "";
            if($headclass) $class = " class=\"".$headclass."\"";

            if($headtxt){
                    $errtxt = "<p$class>$headtxt<br><br>";
            }else{
                    $errtxt = "<p$class>";
            }
            foreach($err_arr as $value){
                    $errtxt .= "$value<br>";
            }

            if($headtxt){
                $errtxt .= "</p>";
            }
	}

    return $errtxt;
}



/**
 *  文字コード変換
 *
 *  @param  string $txt
 *  @param  string $convert_to_code
 *  @param  string $convert_from_code
 *  @return string
 */
function fn_change_stringcode($txt,$convert_to_code,$convert_from_code){

	if(STRINGCODE_CHANGE == true && $convert_to_code != $convert_from_code){
		return mb_convert_encoding($txt,$convert_to_code,$convert_from_code);
	}

	return $txt;
}


/**
 * IP取得
 *
 * @return String
 */
function fn_getIP(){

	$ip = "";
	if(isset($_SERVER['REMOTE_ADDR'])) $ip=(isset($_SERVER['HTTP_PC_REMOTE_ADDR']) ? $_SERVER['HTTP_PC_REMOTE_ADDR'] : $_SERVER['REMOTE_ADDR']);
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ //プロキシ
		$real_ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		if(ereg('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$',$real_ip)) $ip=$real_ip;
	}
	return $ip;
}

/**
 * User Agent情報取得
 *
 * @return String
 */
function fn_getUA(){

	return $_SERVER["HTTP_USER_AGENT"];
}

/**
 * デバッグ用
 *
 * @param data $var
 */
function pr($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}


/**
 * ユニーク値を返す
 *
 */
function uniq() {
	return sha1(uniqid( mt_rand() , true ));
}

/**
* かっこよくなる関数追加
*/
function with($obj) {
    return $obj;
}

if ( ! function_exists( 'uniq2' ) ) {
	function uniq2($len = 32)
	{
		// 文字列を変数代入
		$seed = "3456789abcdefghjikmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXY";

		do {
			// ランダムに１文字取得×８
			$password = "";
			for ($i = 0; $i < $len; $i++) {
				$password .= substr($seed, rand(0, strlen($seed) - 1), 1);
			}
		// 数字・英小文字・英大文字をそれぞれ１文字も含まない場合はループ続行
		} while (!ereg("[0-9]", $password) || !ereg("[a-z]", $password) || !ereg("[A-Z]", $password));

		// 出力する
		return htmlspecialchars($password);
	}
}
