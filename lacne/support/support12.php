<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>簡易LPO機能について | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-12',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support12/page_ttl.gif" width="660" height="52" alt="簡易LPO機能について" /></h2>
<p class="lead M-pb00">簡易LPO機能オプションを導入されている場合、記事作成画面にLPOの項目が追加され、そこにLPOの書式を書き込むことで、時間や曜日による表示コンテンツの切り替えを行うことができるようになります。</p>
<p class="lead att M-size-txt"><span class="heighlight">※</span> 記事を表示させるページ側にLPOコンテンツを表示させるための記述を行う必要があります。これは弊社にて行いますので、LPO機能オプションをご希望の場合は、どの位置に表示させたいかなどの情報をお聞かせ下さい。</p>

<div class="blueblock"><div class="blueblock-outline"><div class="blueblock-inline">

<p class="M-align-center M-mb20"><img src="images/support12/capture_img.jpg" width="560" height="220" alt="" /></p>
<p>記事作成画面のLPOの項⽬に、書式を記述します。</p>
<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
