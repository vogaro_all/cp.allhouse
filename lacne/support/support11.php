<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>Twitter、facebook投稿機能について | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support11.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-11',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support11/page_ttl.gif" width="660" height="52" alt="Twitter、facebook投稿機能について" /></h2>
<p class="lead M-pb00">Twitter、facebook投稿機能オプションを導入されている場合、記事を公開されるタイミングで、その記事の情報（タイトルやURL情報）をTwitterとfacebook上に投稿することができます。</p>
<p class="lead att M-size-txt"><span class="heighlight">※</span> 各種設定画面上で、Twitterとfacebookのアカウント認証を行なっておく必要があります。</p>

<div class="blueblock"><div class="blueblock-outline"><div class="blueblock-inline">

<p class="M-align-center"><img src="images/support11/capture_img_01.jpg" width="562" height="129" alt="" /></p>

<div class="in-border"><div class="in-border-outline"><div class="in-border-inline">

<div class="capture capture01 M-mb20">
<p class="M-align-center"><img src="images/support11/capture_img_02.jpg" width="510" height="203" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support11/capture_txt_01.jpg" width="75" height="49" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<p>記事の公開が行われると、そのあとに、ソーシャルメディア投稿画面が表示されます。</p>
<!-- .in-border-inline // --></div><!-- .in-border-outline // --></div><!-- .in-border // --></div>

<p class="M-align-center"><img src="images/support11/capture_img_03.jpg" width="562" height="108" alt="Twitter、facebook投稿画面へ" /></p>

<div class="in-border"><div class="in-border-outline"><div class="in-border-inline">

<div class="capture capture02 M-mb20">
<p class="M-align-center"><img src="images/support11/capture_img_04.jpg" width="510" height="263" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support11/capture_txt_02.jpg" width="75" height="49" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<p>ここで、投稿先（Twitter、facebook）にチェックを入れ、投稿内容を編集（デフォルトで記事のタイトルとURLが挿入されています。最大140文字まで）した後、「投稿する」ボタンをクリックすると、各ソーシャルメディアに投稿が行われます。</p>
<!-- .in-border-inline // --></div><!-- .in-border-outline // --></div><!-- .in-border // --></div>

<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
