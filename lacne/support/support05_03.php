<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>編集操作について | 記事一覧画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support05_03.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-05',{type:'text'});
	$.library.active('sn-05-03',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>

<div id="Main">
<h2><img src="images/support05_03/page_ttl.gif.gif" width="660" height="52" alt="編集操作について" /></h2>

<div class="grayblock"><div class="grayblock-outline"><div class="grayblock-inline">
<p><img src="images/support05_03/capture_img_01.jpg" width="561" height="123" alt="" /></p>

<div class="in-waside M-mb40"><div class="in-waside-outline">
<div class="in-waside-inline">
<ul>
<li><span><img src="images/support05_03/edit_ico_01.gif" width="17" height="17" alt="" /></span>・・・ 記事を編集します。クリックすると、編集画面へ移動します。</li>
<li><span><img src="images/support05_03/edit_ico_02.gif" width="17" height="17" alt="" /></span>・・・ 記事が実際にどのように表示されるかプレビューを行います。</li>
<li><span><img src="images/support05_03/edit_ico_03.gif" width="17" height="17" alt="" /></span>・・・ 記事を削除します。</li>
</ul>
<!-- .in-aside-whblock-inline // --></div><!-- .in-aside-whblock-outline // --></div><!-- .in-aside-whblock // --></div>

<h3 class="circlettl">記事の一括削除について</h3>
<div class="in-border M-mb20"><div class="in-border-outline"><div class="in-border-inline">
<p class="M-mb20"><img src="images/support05_03/capture_img_02.jpg" width="510" height="334" alt="" /></p>
<p>複数の記事を一括して削除したい場合は、一番左に配置されているチェックボックスにチェックを入れ、画面一番下に配置されている「選択記事の削除」ボタンをクリックしてください。</p>
<!-- .in-border-inline // --></div><!-- .in-border-outline // --></div><!-- .in-border // --></div>

<!-- .grayblock-inline // --></div><!-- .grayblock-outline // --></div><!-- .grayblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
