<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>簡易LPOの書式について | 簡易LPO機能について | LACNE CMSサポートガイド</title>


<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-12',{type:'text'});
	$.library.active('sn-12-01',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support12_01/page_ttl.gif" width="660" height="52" alt="簡易LPOの書式について" /></h2>
<p class="lead">簡易LPO機能は下記のような書式でLPOエリア内に条件式を記述します。</p>

<div class="blueblock M-mb50"><div class="blueblock-outline"><div class="blueblock-inline">
<ul class="circle">
<li class="M-mb20">==条件式1（lpo_time:○○-○○ または lpo_days:○-○）<br />この条件にマッチしたときに表示させるHTMLタグ</li>
<li>==条件式1（lpo_time:○○-○○ または lpo_days:○-○）<br />この条件にマッチしたときに表示させるHTMLタグ</li>
</ul>
<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<h3 class="M-align-center M-mb10"><img src="images/support12_01/sample_ttl_01.gif" width="630" height="28" alt="サンプル1" /></h3>
<p class="lead M-pb30">例えば、「10:00～18:00までは画像A」を、「18:00～21:00までは画像B」を、「その他の時間は画像C」を表示させたいという場合</p>
<div class="blueblock M-mb30"><div class="blueblock-outline"><div class="blueblock-inline">
<ul class="circle">
<li class="M-mb20">==lpo_time:1000-1800<br />
&lt;img src=&quot;/画像Aのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像A&quot; /&gt;</li>
<li class="M-mb20">==lpo_time:1800-2100<br />
&lt;img src=&quot;/画像Bのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像B&quot; /&gt;</li>
<li>==lpo_time:<br />
&lt;img src=&quot;/画像Cのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像C&quot; /&gt;</li>
</ul>
<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>
<p class="lead M-pb10">「==lpo_time:」につづいて時間帯を記載し、改行を入れてその下段に出力させるHTMLタグを記載します。</p>

<div class="aside M-mb50"><div class="aside-outline"><div class="aside-inline">
<ul>
<li class="att M-size-txt"><span class="heighlight">※</span> 時間帯の記載がない場合は、その他の登録条件に当てはまらなかった場合の処理として扱われます。</li>
<li class="att M-size-txt"><span class="heighlight">※</span> 画像のパスなどやリンクURLを記載する場合は、http://からはじまるURLなど絶対パス指定で記載してください。</li>
</ul>
<!-- .aside-inline // --></div><!-- .aside-outline // --></div><!-- .aside // --></div>


<h3 class="M-align-center M-mb10"><img src="images/support12_01/sample_ttl_02.gif" width="630" height="28" alt="サンプル2" /></h3>
<p class="lead M-pb30">例えば、「月曜日～木曜日までは画像A」を、「金曜日～土曜日までは画像B」を、「その他の時間は画像C」を表示させたいという場合</p>
<div class="blueblock M-mb30"><div class="blueblock-outline"><div class="blueblock-inline">
<ul class="circle">
<li class="M-mb20">==lpo_days:1-4<br />
&lt;img src=&quot;/画像Aのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像A&quot; /&gt;</li>
<li class="M-mb20">==lpo_days:5-6<br />
&lt;img src=&quot;/画像Bのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像B&quot; /&gt;</li>
<li>==lpo_days:<br />
&lt;img src=&quot;/画像Cのパス&quot; width=&quot;100&quot; height=&quot;100&quot; alt=&quot;画像C&quot; /&gt;</li>
</ul>
<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<p class="lead M-pb10">「==lpo_days:」につづいて曜日Noを記載し、改⾏を⼊れてその下段に出力させるHTMLタグを記載します。<br />
0 : 日曜日 1:月曜日 2:火曜日 3:水曜日 4:木曜日 5:金曜日 6:土曜日 </p>

<div class="aside"><div class="aside-outline"><div class="aside-inline">
<ul>
<li class="att M-size-txt"><span class="heighlight">※</span> 時間帯の記載がない場合は、その他の登録条件に当てはまらなかった場合の処理として扱われます。</li>
<li class="att M-size-txt"><span class="heighlight">※</span> 画像のパスなどやリンクURLを記載する場合は、http://からはじまるURLなど絶対パス指定で記載してください。</li>
</ul>
<!-- .aside-inline // --></div><!-- .aside-outline // --></div><!-- .aside // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
