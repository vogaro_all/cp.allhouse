<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/index.css" media="all" />
<script type="text/javascript">
$(document).ready(function(){
	$('#Main li:odd').addClass('odd');
	$('#Main li:last').addClass('last');
	$.library.pagetop('#PageTop',{positionbottom:28});
});
</script>
</head>

<body>
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/index/page_ttl.gif" width="142" height="25" alt="操作ガイド目次" /></h2>

<div class="toc"><div class="toc-inline">
<ul>
<li><a href="support01.php">管理画面へのログイン</a></li>
<li><a href="support02.php">パスワードの再発行（パスワードリマインダ）</a></li>
<li><a href="support03.php">メインメニュー画面</a></li>
<li><a href="support04.php">インデックス画面</a></li>
<li><a href="support05.php">記事一覧画面</a><span>(  <a href="support05_01.php">公開・非公開操作について</a> /  <a href="support05_02.php">承認、差戻しの操作について</a> /  <a href="support05_03.php">編集操作について</a> )</span></li>
<li><a href="support06.php">記事作成画面</a><span>(  <a href="support06_01.php">リンク先、メタ情報の入力について</a> /  <a href="support06_02.php">記事詳細 について</a> /  <a href="support06_03.html">記事詳細 エディタ操作について</a> /  <a href="support06_04.php">記事詳細 エディタ上での画像の挿入について</a> )</span></li>
<li><a href="support07.php">カテゴリ管理画面</a></li>
<li><a href="support08.php">メディアアップロード画面</a></li>
<li><a href="support09.php">アカウント管理画面</a><span>(  <a href="support09_01.php">アカウントの作成画面について</a> /  <a href="support09_02.php">アカウントの権限タイプについて</a> )</span></li>
<li><a href="support10.php">各種設定画面</a></li>
<li><a href="support11.php">Twitter、facebook投稿機能について</a></li>
<li><a href="support12.php">簡易LPO機能について</a><span>(  <a href="support12_01.php">簡易LPOの書式について</a> )</span></li>
<li><a href="support13.php">スマートフォン用管理画面での注意点について</a></li>
</ul>
<!-- .toc-inline // --></div><!-- .toc // --></div>

<!-- #Main // --></div>

<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>

<!-- #Container // --></div>
</body>
</html>
