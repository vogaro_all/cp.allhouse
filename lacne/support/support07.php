<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>カテゴリ管理画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support07.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-07',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support07/page_ttl.gif" width="660" height="52" alt="カテゴリ管理画面" /></h2>
<p class="lead">カテゴリの作成や管理を⾏うための画面です。<br />
「新規作成」ボタンをクリックすると、新しいカテゴリの作成画面が表示されます。<br />
カテゴリ一覧には、登録されているカテゴリが表示され、編集や削除を行うことができます。</p>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<div class="capture capture01">
<p class="M-align-center"><img src="images/support07/capture_img_01.jpg" width="518" height="247" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support07/capture_txt_01.jpg" width="76" height="44" alt="クリック！" /></li>
<li class="popup02"><img src="images/support07/capture_txt_02.jpg" width="248" height="115" alt="カテゴリの削除を行います。※ 削除するカテゴリに属している記事は、すべて別カテゴリに移動されます。（一覧の一番上にあるカテゴリに移動します）" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline2">
<p class="M-mb20">カテゴリの新規作成や、登録済みカテゴリ名の編集を行います。</p>
<div class="capture capture01">
<p class="M-align-center"><img src="images/support07/capture_img_02.jpg" width="517" height="281" alt="" /></p>
<!-- .capture // --></div>
<!-- .blueblockarw-inline2 // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
