<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>スマートフォン用管理画面での注意点について | LACNE CMSサポートガイド</title>


<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support13.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-13',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support13/page_ttl.gif" width="660" height="52" alt="スマートフォン用管理画面での注意点について" /></h2>
<p class="lead M-pb00">スマートフォン用管理画面は、利用できる機能に関してはPC版とほぼ同一です。<br />
スマートフォン用に表示を最適化しているため、表示のレイアウトが異なっていたり、一部操作方法が異なる箇所や操作を行うことができない箇所があります。</p>
<p class="lead att M-size-txt"><span class="heighlight">※</span> スマートフォン用管理画面オプションを導入されている場合のみご利用可能です。</p>

<div class="section M-mb50">
<h3 class="M-align-center M-mb10"><img src="images/support13/section_ttl_01.gif" width="630" height="28" alt="1 . ファイルのアップロードについて（メディアアップロード）" /></h3>
<p class="lead M-pb00">iPhoneをご利用の場合、ブラウザからファイルのアップロードを行うことができない仕様であるため、管理画面のメディアアップロード画面にてファイルをアップロードする機能を利用することができません。</p>
<!-- .section // --></div>

<div class="section M-mb50">
<h3 class="M-align-center M-mb10"><img src="images/support13/section_ttl_02.gif" width="630" height="28" alt="2 . 記事作成画面での記事詳細のエディタ機能について" /></h3>
<p class="lead M-pb30">Android端末では、OS仕様により、エディタ機能を利用することができません。ソースコードの編集モードで直接本文の編集を行っていただくかたちになります。</p>
<div class="blueblockarw"><div class="blueblockarw-outline"><div class="blueblockarw-inline2">
<p><img src="images/support13/capture_img_01.jpg" width="520" height="440" alt="" /></p>
<!-- .blueblockarw-inline2 // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>
<!-- .section // --></div>

<div class="section">
<h3 class="M-align-center M-mb10"><img src="images/support13/section_ttl_03.gif" width="630" height="50" alt="3. 記事一覧画面上で、選択した記事の削除、または、公開切り替え、承認・差戻しの操作手順がPC版と異なります。" /></h3>
<p class="lead M-pb30">記事の削除や、公開状態の切り替え、承認・差戻しの操作を行う場合は、</p>

<div class="blueblockarw M-mb15"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<h4 class="num01">まず、記事一覧画面上で、操作対象となる記事タイトル横のチェックボックスにチェックを入れます。</h4>
<div class="capture capture01">
<p class="M-align-center"><img src="images/support13/capture_img_02.jpg" width="520" height="223" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support13/capture_txt_01.jpg" width="75" height="49" alt="チェック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb15"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<h4 class="num02">一覧の上部にあるプルダウンメニューで、「記事を公開 / 非公開」「記事を承認 / 差戻し」「記事を削除」の中から操作を選択します。</h4>
<div class="capture capture02">
<p class="M-align-center"><img src="images/support13/capture_img_03.jpg" width="520" height="343" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support13/capture_txt_02.jpg" width="262" height="309" alt="※ 複数の記事を選択した状態で「公開 / 非公開」の操作、または「承認 / 差戻し」の操作を行うことはできません。（削除は可能です）" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb15"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<h4 class="num03">「GO」ボタンをクリックすると、操作の確認画面へ移動します。</h4>
<div class="capture capture03">
<p class="M-align-center"><img src="images/support13/capture_img_04.jpg" width="520" height="223" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support13/capture_txt_03.jpg" width="75" height="48" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<!-- .section // --></div>

<div class="aside"><div class="aside-outline"><div class="aside-inline">
<p>確認を行うと、プルダウンで選択した操作（削除 / 公開 / 承認・差戻し）が完了します。</p>
<p class="att"><span class="heighlight">※</span> 承認・公開権限の機能オプションを導⼊されている場合のみ、「承認 / 差戻し」の操作が有効になります。</p>
<!-- .aside-inline // --></div><!-- .aside-outline // --></div><!-- .aside // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
