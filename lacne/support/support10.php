<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>各種設定画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support10.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-10',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support10/page_ttl.gif" width="660" height="52" alt="各種設定画面" /></h2>
<p class="lead">パスワードの変更を行うことができます。<br />
その他、Twitter、facebook投稿機能オプションを導⼊されている場合は、この画面にてTwitterとFacebookのアカウント認証や設定を行います。</p>

<div class="capture">
<p class="M-align-center"><img src="images/support10/capture_img.jpg" width="600" height="755" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support10/capture_txt_01.jpg" width="238" height="54" alt="ログインパスワードの変更を行います。" /></li>
<li class="popup02"><img src="images/support10/capture_txt_02.jpg" width="356" height="69" alt="Twitter、facebook投稿機能オプションを導⼊されている場合は、ここでアカウント認証や設定を行います。" /></li>
</ul>
<!-- .capture // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
