<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>記事詳細 エディタ上での画像の挿入について | 記事作成画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support06_04.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-06',{type:'text'});
	$.library.active('sn-06-04',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support06_04/page_ttl.gif" width="660" height="52" alt="記事詳細 エディタ上での画像の挿入について" /></h2>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<p class="M-mb20">画像の挿入を行いたい箇所にカーソルを合わせ、画像挿入ボタンをクリックして下さい。<br />
画像挿入用のウインドウが表示されます。</p>
<div class="capture capture01">
<p class="M-align-center"><img src="images/support06_04/capture_img_01.jpg" width="518" height="183" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06_04/capture_txt_01.jpg" width="75" height="47" alt="クリック！" /></li>
<li class="popup02"><img src="images/support06_04/capture_txt_02.jpg" width="112" height="51" alt="画像挿入位置" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<p class="M-mb20">「参照」をクリックすると、ファイル一覧が表示されます。</p>
<div class="capture capture02">
<p class="M-align-center"><img src="images/support06_04/capture_img_02.jpg" width="518" height="204" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06_04/capture_txt_03.jpg" width="75" height="47" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<p class="M-mb20">挿入したいファイルをクリックして下さい。</p>
<div class="capture capture03">
<p class="M-align-center"><img src="images/support06_04/capture_img_03.jpg" width="518" height="364" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06_04/capture_txt_04.jpg" width="75" height="50" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<p class="M-mb20">選択したファイルのパスが表示されます。<br />
その他、画像の代替テキストやサイズなどを入力して、調整を行うことができます。</p>
<div class="capture capture04 M-mb20">
<p class="M-align-center"><img src="images/support06_04/capture_img_04.jpg" width="518" height="514" alt="" /></p>
<ul>
<li class="popup03"><img src="images/support06_04/capture_txt_07.jpg" width="75" height="49" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>

<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="aside"><div class="aside-outline"><div class="aside-inline">
<p class="M-mb10">OKボタンをクリックすると、画像が挿入されます。</p>
<p class="att"><span class="heighlight">※</span> 挿入した画像を編集したい場合は、エディタ上の画像をダブルクリックするか、または、クリックした後、画像挿入ボタンをクリックして下さい。</p>
<!-- .aside-inline // --></div><!-- .aside-outline // --></div><!-- .aside // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
