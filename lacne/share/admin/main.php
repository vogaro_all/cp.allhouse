<?php

/**------------------------------------------------------------------------
 *  
 *  総合メイン
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

$render_data = array(
    "login_id" => $login_id
);

//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のmain/以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/main");

//Render
$LACNE->render("main" , $render_data);

?>