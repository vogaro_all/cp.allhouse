<?php

/**------------------------------------------------------------------------
 *  
 *  Twitter認証
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login' , 'setting')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

//Social連携オプション
if(method_exists($LACNE->library["setting"] , 'get_connect_info'))
{
    //接続情報を取得
    $connect_info = $LACNE->library["setting"]->get_connect_info();
    
    //すでにアカウント認証しているかどうか
    if(isset($connect_info["twitter"]["account"]) && $connect_info["twitter"]["account"])
    {
        //認証済みならCLOSE
    }
    else
    {
        //認証画面へ
        fn_redirect("twitter_auth.php");
    }
    
}
else
{
    exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Twitter認証 | LACNE（ラクネ）</title>

<script type="text/javascript">
if( window.opener && !window.opener.closed ){
    window.opener.location.reload();
}
window.close();
</script>
</head>
<body>
認証済みです。
</body>
</html>