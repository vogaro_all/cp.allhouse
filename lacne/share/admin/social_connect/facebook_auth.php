<?php
use lacne\core\model\SocialConnect;
/**------------------------------------------------------------------------
 *  
 *  facebook認証
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login' , 'setting')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

//Social連携オプション
if(method_exists($LACNE->library["setting"] , 'get_connect_info'))
{
    //接続情報を取得
    $connect_info = $LACNE->library["setting"]->get_connect_info();
}
else
{
    exit;
}

//設定画面でTwitterのCONSUMER KEYを登録していれば
if(isset($connect_info["facebook"]["key1"]) && $connect_info["facebook"]["key1"] && isset($connect_info["facebook"]["key2"]) && $connect_info["facebook"]["key2"])
{
    $facebook = new Facebook(array(
        'appId' => $connect_info["facebook"]["key1"],    // App ID/API
        'secret' => $connect_info["facebook"]["key2"],    // アプリの秘訣
        'cookie' => true,
    ));
    
}
else
{
    die("Facebookのアプリケーション登録をおこなって頂き、App IDとApp Secret,account_id を登録して下さい。");
}

$user = $facebook->getUser();
 
if ( !$user ) {
  $loginUrl = $facebook -> getLoginUrl(
    array(
      'canvas'    => 1,
      'fbconnect' => 0,
      'scope'     => 'status_update,publish_stream,manage_pages,offline_access'
      )
    );
 
  header( 'Location: ' . $loginUrl );
  exit();
}


try {
    
    $user_token = $facebook->getAccessToken();
    $account = "me";
    $userProfile = $facebook->api('/me/accounts');
    $userProfile = (array)$userProfile; //2012.04.18 add
    
    $tid = $connect_info["facebook"]["post_target_id"]; //投稿対象とするID(facebookページ）
    
    //投稿対象とするID(facebookページ）が自身のアカウントデータ内に含まれているか
    //あればそのアクセストークンを取得してセット
    if($tid)
    {
        foreach($userProfile['data'] as $data){
                $data = (array)$data; //2012.04.18 add
                if($data['id'] == $tid){
                        $user_token = $data['access_token'];
                        $account = $tid;
                        break;
                }
        }
    }
    //$page = $facebook->api('/'.$tid.'?fields=access_token'); //対象とするアカウントID(user or facebookページ）のアクセストークンを取得する
  
    //if(isset($page["access_token"]) && $page["access_token"] && isset($page["id"]) && $page["id"])
    if($user_token)
    {
        $updata = array(
            "type" => "facebook",
            //"token1" => $page["access_token"],
            "token1" => $user_token,
            "token2" => "",
            //"account" => $page["id"],
            "account" => $account,
            "modified" => fn_get_date()
        );

        with(new SocialConnect())->replace($updata , "type");

        fn_redirect("facebook_index.php");
    }
    else
    {
      die("認証時に予期せぬエラーが発生しました。再度お試し下さい。");
    }
  
} catch (FacebookApiException $e) {
  echo  $e->getMessage();
  exit();
}
