
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @fileOverview Definition for placeholder plugin dialog.
 *
 */

'use strict';

CKEDITOR.dialog.add( 'placeholder', function( editor ) {
	var lang = editor.lang.placeholder,
		generalLabel = editor.lang.common.generalTab,
		validNameRegex = /^[^\[\]\<\>]+$/;

	return {
		title: lang.title,
		minWidth: 300,
		minHeight: 150,
		contents: [
			{
				id: 'info',
				label: generalLabel,
				title: generalLabel,
				elements: [
					// Dialog window UI elements.
					{
						id: 'txtUrl',
						type: 'text',
						style: 'width: 100%;',
						label: lang.name,
						'default': '',
						required: true,
						validate: CKEDITOR.dialog.validate.regex( validNameRegex, lang.invalidName ),
						setup: function( widget ) {
							this.setValue( getMovieVal(widget , 1) );
						},
						commit: function( widget ) {
							widget.setData( 'name', this.getValue() );
						}
					},
  					{
						type: 'button',
						id: 'browse',
						style: 'display:inline-block;',
						align: 'center',
						label: '参照',
						onClick : function() {
						  imgSelect('movie');
						}
					},
					{
						type: 'hbox',
						widths: ['15%', '80%'],
						children: [{
							id: 'width',
							type: 'text',
							label: 'width',
							'default': '',
							style: 'width:50px',
							validate: CKEDITOR.dialog.validate.integer('値が数値ではありません'),
							setup: function (widget) {
								this.setValue( getMovieVal(widget , 2) );
							},
							commit: function( widget ) {
								widget.setData( 'width', this.getValue() );
							}
						}, {
							id: 'height',
							type: 'text',
							label: 'height',
							'default': '',
							style: 'width:50px',
							validate: CKEDITOR.dialog.validate.integer('値が数値ではありません'),
							setup: function (widget) {
								this.setValue( getMovieVal(widget , 3) );
							},
							commit: function( widget ) {
								widget.setData( 'height', this.getValue() );
							}
						}]
					}
				]
			}
		]
	};
		
		
    function getMovieVal(val , get_no)
    {
    	if(val.data.name !== undefined)
		{
			if(val.data.width !== undefined && val.data.height !== undefined)
			{
				if(get_no == 1) return val.data.name;
				else if(get_no == 2) return val.data.width;
				else if(get_no == 3) return val.data.height;
			}
			else
			{
		        //var z1 = val.slice(2, -2);
		        var z2 = val.data.name.split("_|_");
		        if(get_no == 1 || get_no == 2 || get_no == 3)
		        {
		            if(z2[get_no] !== undefined)
		            {
		                return z2[get_no];
		            }
		            else
		            {
		            	//width or height
		            	if(get_no == 2 || get_no == 3)
		            	{
		                	return 300;
		                }
		            }
		        }
		    }
		}
    };
} );