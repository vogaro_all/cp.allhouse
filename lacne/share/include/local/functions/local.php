<?php

/**
 * ローカル用共有関数
 *
 * @package  Lacne
 * @author  In Vogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- In Vogue Inc. All rights reserved.
 */

/**
 *  リダイレクト
 * 
 *  @param  string $url 移動先URL
 *  @param  bool $is301
 *  @return void
 */
/*
if( !function_exists('fn_redirect') )
{
    function fn_redirect( $url, $is301 = FALSE )
    {
            if( $is301 )
            {
                    header( "HTTP/1.1 301 Moved Permanently" );
            }
            header( "Location: " . $url );
            exit();
    }
}
*/

