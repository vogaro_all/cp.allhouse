<?php

    $page_setting = array(
        "title" => "各種設定",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/setting/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    $("input[name=ipaddress]")
    .focus(function(){
        $("#ip-alert-container").fadeTo('fast' , 0.95);
    })
    .blur(function(){
        $("#ip-alert-container").fadeOut('fast');
    });
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("err"=>$err , "message"=>$message));
?>


<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>各種設定</h1>
<p class="load">パスワードの変更や各種設定を行います。</p>
<?php if(isset($err) && $err) : ?>
<div class="alert error pie" id="comp_message" style="display:none;margin-bottom:5px"><span class="icon">エラー</span><p class="fl"><?=$err?></p></div>
<?php elseif(isset($message) && $message) : ?>
<div class="alert comp pie" id="comp_message" style="display:none;margin-bottom:5px"><span class="icon">完了</span><p class="fl"><?=$message?></p></div>
<?php endif; ?>
<!-- .section // --></section>

<section class="section">
<h2 class="head-line02">パスワードの変更</h2>
<form action="" method="post" class="section-inside">
<p><span class="label">現在のパスワード：</span><input type="password" name="password" class="text" /></p>
<p><span class="label">新しいパスワード：</span><input type="password" name="new_password" class="text" /></p>
<p><span class="label">新しいパスワード確認：</span><input type="password" name="new_password_conf" class="text" /></p>
<p class="btn-type01 pie"><input type="submit" name="password_change" value="更新" class="pie" /></p>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>

<?php
// ------------------------------------------------------------------------
// 
// IPアドレス制限設定
// 
// ------------------------------------------------------------------------
if(isset($ipaddress)) {
?>
<section class="section">
<h2 class="head-line02">IP制限</h2>
<p class="load">現在ログインしているアカウントに対して、ログインを許可する接続元IPアドレスの設定を行います。
<?php
//現在アクセスしているIPアドレスは含まれているかチェック
if(!empty($ipaddress) && empty($err)) {
    $this_ip = fn_getIP();
    $input_ip = explode("," , $ipaddress);
    if(!in_array($this_ip , $input_ip)) {
?>
<br>
<strong style="color:#FF3300">現在アクセスされているIPアドレスが、登録された接続元IPアドレスリストに含まれていません。<br>次回以降のログインの際にはご注意下さい。</strong><br>
<?php
    }
}
?>
</p>

<form action="" method="post" class="section-inside">
<p><span class="label">IPアドレス：</span><input type="text" name="ipaddress" class="text" value="<?php if(!empty($ipaddress)){ echo $ipaddress; } ?>" /></p>
<div id="ip-alert-container">
<div id="ip-alert">
※許可するIPアドレスを入力して下さい。<br>入力が空の場合は制限は行われません</b>。<br>
※複数のIPアドレスを登録する場合は<b>カンマ区切り</b>で入力して下さい。<br>
※必ず<b>固定化されているIPアドレス</b>を登録するようにして下さい。<br>
（特にスマートフォンなどからログインされる事がある場合は、LTEや3G通信利用時にIPアドレスが変動しますのでログインできなくなる可能性があります。）
</div>
</div>
<p class="btn-type01 pie"><input type="submit" name="ipaddress_lock" value="更新" class="pie" /></p>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>
<?php
}
?>

<?php
// ------------------------------------------------------------------------
// 
// 以下、その他設定関連
// 
// ------------------------------------------------------------------------

//権限チェック（以下の処理は権限が必要）
if($LACNE->library["login"]->chk_controll_limit("manage_setting")) :
?>

<?php
//SNS連携オプションが有効かどうか
if(method_exists($LACNE->library["setting"], "get_connect_info")) :
?>
<section class="section auth">    
<h2 class="head-line02">ソーシャルアカウントの設定</h2>
<form action="" method="post" class="section-inside">
<div class="section-inside">
<p class="twitter"><span class="auth-txt">Twitter：</span>
<?php
if(!empty($connect_info["twitter"]["key1"]) && !empty($connect_info["twitter"]["key2"])) :
    //アカウント認証済みなら
    if(isset($connect_info["twitter"]["account"]) && $connect_info["twitter"]["account"]) :
?>    
<span class="outh">認証済み（<a href="https://twitter.com/<?=$connect_info["twitter"]["account"]?>" target="_blank"><?=$connect_info["twitter"]["account"]?></a>）</span>
    <a href="setting.php?action=auth_off&target=twitter">認証を解除</a>
<?php
    else:
?>
<a href="javascript:void(0)" id="outh_btn_twitter" style="cursor:pointer" onClick="OpenNewWindow('win1','<?=LACNE_APP_ADMIN_PATH?>/social_connect/twitter_index.php','width=500,height=500,toolbar=no,location=yes,menubar=no,scrollbars=yes');return false;">認証する</a>
<?php
    endif;
else:
?>
<span>下記2つのKEY登録をおこなって下さい。</span>
<?php
endif;
?>
</p>
<p class="keys"><span class="label">CONSUMER_KEY：</span><input type="text" name="social[TWITTER_CONSUMER_KEY]" maxlength="50" class="text" value="<?=(isset($connect_info["twitter"]["key1"]))?$connect_info["twitter"]["key1"]:""?>" /></p>
<p class="keys"><span class="label">CONSUMER_SECRET：</span><input type="text" name="social[TWITTER_CONSUMER_SECRET]" maxlength="50" class="text" value="<?=(isset($connect_info["twitter"]["key2"]))?$connect_info["twitter"]["key2"]:""?>" /></p>

<p class="facebook"><span class="auth-txt">Facebook：</span>
<?php 
if(!empty($connect_info["facebook"]["key1"]) && !empty($connect_info["facebook"]["key2"])) :
    //アカウント認証済みなら
    if(isset($connect_info["facebook"]["account"]) && $connect_info["facebook"]["account"]) :
?>
<span class="outh">認証済み（<a href="https://facebook.com/<?=$connect_info["facebook"]["account"]?>" target="_blank"><?=$connect_info["facebook"]["account"]?></a>）</span>
    <a href="setting.php?action=auth_off&target=facebook">認証を解除</a>
<?php
    else:
?>
<a href="javascript:void(0)" id="outh_btn_facebook" style="cursor:pointer" onClick="OpenNewWindow('win1','<?=LACNE_APP_ADMIN_PATH?>/social_connect/facebook_index.php','width=500,height=500,toolbar=no,location=yes,menubar=no,scrollbars=yes');return false;">認証する</a>
<?php
    endif;
else:
?>
<span>下記2つのKEY登録をおこなって下さい。</span>
<?php
endif;
?>
</p>

<p class="keys"><span class="label">APP_ID：</span><input type="text" name="social[FACEBOOK_APP_ID]" maxlength="50" class="text" value="<?=(isset($connect_info["facebook"]["key1"]))?$connect_info["facebook"]["key1"]:""?>" /></p>
<p class="keys"><span class="label">APP_SECRET：</span><input type="text" name="social[FACEBOOK_APP_SECRET]" maxlength="50" class="text" value="<?=(isset($connect_info["facebook"]["key2"]))?$connect_info["facebook"]["key2"]:""?>" /></p>
<p class="keys"><span class="label">POST_TARGET_ID：</span><input type="text" name="social[FACEBOOK_POST_TARGET_ID]" maxlength="50" class="text" value="<?=(isset($connect_info["facebook"]["post_target_id"]))?$connect_info["facebook"]["post_target_id"]:""?>" /></p>

<!-- .section-inside // --></div>
<p class="btn-type01 pie"><input type="submit" name="social_data_change" value="更新" class="pie" /></p>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>
<?php
endif;
?>

<?php
// ------------------------------------------------------------------------
// その他設定関連ここまで
// ------------------------------------------------------------------------
endif;
?>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>