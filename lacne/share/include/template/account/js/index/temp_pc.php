<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.tooltip({margin_x:-25});
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $('a[href=#box-regist]').click(function(){$('#box-regist iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/account/register.php');});
    $('#Pc #Main .section .table-list thead input.master').click(function(){
            $('#Pc #Main .section .table-list tbody input[type=checkbox]').prop('checked', this.checked);
    });
    $('#Pc #Main .section .table-list tbody tr').hover(function(){
            $(this).addClass('highlight');
    },function(){
            $(this).removeClass('highlight');
    });

    //編集ボタン
    $('.btn_edit').click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("edit_" , "");
        $('#box-regist iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/account/register.php?id='+tid);
        $.library.modalOpen($("<a>").attr("href","#box-regist"),"#Modal");
    });
    
});


<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    stripe_list();
    
    $.library.css3('#Main .section .table-list thead th,#Main .section .table-list thead td','<?=LACNE_SHAREDATA_PATH?>/');
    
    
    //削除ボタン
    $('.btn_del').click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("del_" , "");
        $.ajax({
            type:'POST',
            url : '<?=LACNE_APP_ADMIN_PATH?>/account/index.php',
            data:'account_name='+tid,
            success : function(result)
            {
                if(result)
                {
                    $('#box-delete a#delete_link').attr('href',tid);
        
                    $('#delete_account').html(result);
                    $.library.modalOpen($("<a>").attr("href","#box-delete"),"#Modal");
                }
            }
        });
    });
    $('#box-delete a#delete_link').click(function(){
        var tid = $(this).attr("href");
        $('input[type=checkbox]').attr("checked",false); 
        $('input[type=checkbox][value='+tid+']').attr("checked","checked");
        $('form[name=del_form]').submit();
        return false;
    });
    
    //複数削除
    $('#btn_dels').click(function(){
        //削除にチェックされているか
        if($("input.delete_check:checked")[0])
        {
            $.library.modalOpen($("<a>").attr("href","#box-delete02"),"#Modal");
        }
        else
        {
            $("#error_message").html("削除するアカウントにチェックを入れて下さい。");
            $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
        }
    });
    $('#run-delete').click(function(){
        $('form[name=del_form]').submit();
    });


});


function stripe_list()
{
    //しましま表示
    $('#Main .section .table-list tbody tr:last').addClass('last');
    $('#Main .section .table-list tbody tr:nth-child(even)').addClass('odd');
}


// -----------------------------------------------
</script>
