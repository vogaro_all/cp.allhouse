<?php

/**------------------------------------------------------------------------
 *  
 *  iphoneアプリ用API
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------


/** iphoneアプリ用設定パス */
//  [_CHECK_] アプリ対応させるコンテンツのID、setupファイルのパス指定
$LACNE_IAPP_SETTING = array(
    "contents" => array(
        //初期のコンテンツ指定がない時点ではID:1のsetup.phpをロードする
        //そのsetup.phpが含まれるコンテンツのテーブル情報をもとにアカウント認証をおこなうため、
        //ログインをおこなうコンテンツのsetup.phpをID:1に指定するのがよい
        "1" => array("name" => "新着ニュース管理" , "setup_file" => dirname(__FILE__)."/../../news/include/setup.php"),
        "2" => array("name" => "商品情報管理" , "setup_file" => dirname(__FILE__)."/../../item/include/setup.php")
    )
);

//content_id指定があればそのIDのコンテンツsetup.phpをロード
$content_id = 1;
if(isset($_POST["content_id"]) && is_numeric($_POST["content_id"]))
{
    $content_id = $_POST["content_id"];
}

if(!isset($LACNE_IAPP_SETTING["contents"][$content_id]["setup_file"])) 
{
    exit;
}

$setup_file = $LACNE_IAPP_SETTING["contents"][$content_id]["setup_file"];
require_once($setup_file);

//テンプレートファイルのリスト取得
if(!isset($IAPP_TEMPLATE_LIST) || !is_array($IAPP_TEMPLATE_LIST))
{
    $IAPP_TEMPLATE_LIST = array();
}


// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('App_api')); //library load


$response_data = array();

if(isset($_POST["mode"]) && $_POST["mode"])
{
    switch($_POST["mode"])
    {
        case "login":
            $response_data = $LACNE->library["App_api"]->action_login();
            break;
        
        case "get_contents":
            $response_data = $LACNE->library["App_api"]->action_get_contents($LACNE_IAPP_SETTING["contents"]);
            break;
        
        case "get_data":
            $response_data = $LACNE->library["App_api"]->action_get_data();
            break;
        
        case "get_category":
            $response_data = $LACNE->library["App_api"]->action_get_category();
            break;
        
        case "get_template":
            $response_data = $LACNE->library["App_api"]->action_get_template($IAPP_TEMPLATE_LIST);
            break;
        
        case "img_upload":
            $response_data = $LACNE->library["App_api"]->action_img_upload();
            break;
        
        case "data_upload":
            $response_data = $LACNE->library["App_api"]->action_data_upload($IAPP_TEMPLATE_LIST);
            break;
        
    }
}

if($response_data)
{
    $LACNE->library["App_api"]->output_xml($response_data);
}


?>