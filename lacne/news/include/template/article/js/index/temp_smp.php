<script type="text/javascript">
<?php
if($device == "smp") :
// -----------------------------------------------
// smpの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.boxhover('#Smp #Main .section ul.list div.text');
                
    //カテゴリ選択
    $("#smp_select_cat").change(function(){
        var cat = $(this).val();
        var url = '<?=LACNE_APP_ADMIN_PATH?>/article/index.php?category='+cat+'&<?=fn_set_urlparam($_GET, array('wait'),false)?>';
        window.location.href = url;
    })

    //公開/非公開、承認/差戻し、削除アクション
    $("#btn_go").click(function(){
        //チェックされているか
        var check_num = $("input.delete_check_smp:checked").size();
        if(check_num)
        {

            //選択された処理はどれか
            var smp_action = $("#smp_action").val();
            if(smp_action == 'publish') //公開/非公開
            {
                //複数一括処理には対応していない
                if(check_num > 1) 
                {
                    $("#error_message").html("複数の<?=KEYWORD_KIJI?>を選択した状態で、公開/非公開の切り替え、または、承認/差戻しの処理を行うことはできません。");
                    $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
                }
                else
                {
                    var tid = $("input.delete_check_smp:checked").val();
                    window.location.href = '<?=LACNE_APP_ADMIN_PATH?>/article/publish.php?id='+tid+'&<?=fn_set_urlparam($_GET, array('category','page','sort','wait'),false)?>';
                }
            }
            else if(smp_action == 'app') //承認/差戻し
            {
                //複数一括処理には対応していない
                if(check_num > 1) 
                {
                    $("#error_message").html("複数の<?=KEYWORD_KIJI?>を選択した状態で、公開/非公開の切り替え、または、承認/差戻しの処理を行うことはできません。");
                    $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
                }
                else
                {
                    var tid = $("input.delete_check_smp:checked").val();
                    window.location.href = '<?=LACNE_APP_ADMIN_PATH?>/article/publish.php?action=app&id='+tid+'&<?=fn_set_urlparam($_GET, array('category','page','sort','wait'),false)?>';
                }
            }
            else if(smp_action == 'delete') //削除
            {
                $.library.modalOpen($("<a>").attr("href","#box-delete02"),"#Modal");
            }
        }
        else
        {
            $("#error_message").html("操作対象とする<?=KEYWORD_KIJI?>にチェックを入れて下さい。");
            $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
        }
    });

    //削除モーダルでOKクリックの場合
    $('#run-delete').click(function(){
        $('form[name=del_form_smp]').submit();
    });


    
    
});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){


});
// -----------------------------------------------
</script>
