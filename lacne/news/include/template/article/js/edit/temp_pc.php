<script type="text/javascript">

var CK_toolbar = [
    ['Source','-','Undo','Redo','-','Bold','Italic','Underline','Strike','RemoveFormat','-',
    'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','-','Image','Syntaxhighlight','-','CreateDiv','ShowBlocks'],'/',
    ['Format','FontSize','-','TextColor']
];
var CK_toolbar_movie = [
    ['Source','-','Undo','Redo','-','Bold','Italic','Underline','Strike','RemoveFormat','-',
    'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','-','Image','CreatePlaceholder','-','CreateDiv','ShowBlocks'],'/',
    ['Format','FontSize','-','TextColor']
];
    
var CK_extraplugins = 'justify,font,colorbutton,div,showblocks,lineutils';

<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    Setting_toolbar = CK_toolbar;
    if(INSERT_MOVIE) Setting_toolbar = CK_toolbar_movie; //動画パネル
    CK_EDITOR_HEIGHT = 450; //エディタの高さ指定 
    CK_EDITOR_WIDTH = 700; //エディタのデフォルト幅指定 
    
    //timepicker
    $('#datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'hh:mm',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        timeText:'時刻指定',
        hourText:'時',
        minuteText:'分',
        currentText:'現時刻',
        closeText:'確定'
    });

});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    //[_CHECK_] CSS STYLE選択リストに下記のCLASS STYLEを追加する（無い場合は配列を空に）
    var LACNE_PARTS_CSS =  [
        { name : '大見出し' ,	element : 'h2',		attributes: { 'class' : 'title' } },
        { name : 'ボックス' ,		element : 'div',	attributes: { 'class' : 'box' } },
        { name : 'テキスト',		element : 'p',		attributes: { 'class' : 'text' } },
        { name : 'リンクボタン',	element : 'p',		attributes: { 'class' : 'link' } }
      ];
    //[_CHECK_] エディタ内で適用させるCSS FILEのパス指定（配列 elrte-inner.cssは必須）
    var LACNE_PAGE_CSS_FILE = [
        '<?=LACNE_SHAREDATA_PATH?>/js/ckeditor/contents.css',
        '/corpo/assets/styles/pages/news/detail/style.css'
    ]
    
    //スタイル追加
    if(LACNE_PARTS_CSS.length > 0)
    {
        CKEDITOR.stylesSet.add( 'default',LACNE_PARTS_CSS );
    }
    
	if($("#edit-p").size()>0)
	{
		// create editor
		var editor = CKEDITOR.replace( 'edit-p',
		{
			language : 'ja',
			enterMode : CKEDITOR.ENTER_BR,
			contentsCss : LACNE_PAGE_CSS_FILE,
			width : CK_EDITOR_WIDTH,
			resize_maxWidth : 700,
			height : CK_EDITOR_HEIGHT,
			extraPlugins : CK_extraplugins,
			toolbar : Setting_toolbar,
			filebrowserBrowseUrl: '../media/list.php',
			filebrowserImageBrowseUrl: '../media/list.php?image=1'
		});
	}
	
    //click preview button
    var preview_url = '<?=$LACNE->url_setting->get_pageurl($data_list["id"],$data_list["category"])?>';
    var submit_url = '<?=LACNE_APP_ADMIN_PATH?>/article/edit.php?action=submit&<?=fn_set_urlparam($_GET , array('id') , false)?>';

    $("#preview").css("cursor","pointer").click(function(){
		window.open("about:blank","lacne_preview");
		$("form").attr({"action":preview_url , 'target':'lacne_preview'}).submit();
    });

    //click regstered button
    $("#submit").on('click' , function(){
        $(this).off('click');
        $("form").attr({"action":submit_url, 'target':'_self'}).submit();
    }); 

    //click cancel button
    $("#cancel").click(function(){
        window.location.href = "<?=LACNE_APP_ADMIN_PATH?>/article/index.php";
    });

    //詳細ON / OFF
    disp_detail_area(1);
    $("input[name^=detail_on]").change(function(){
        disp_detail_area($("input[name^=detail_on]:checked").val());
    });

    //詳細クリア
    $("#clear").click(function(){
         $.library.modalOpen($("<a>").attr("href","#box-clear"),"#Modal");
         return false;
    })
    $("#delete_link").click(function(){
        editor.setData( '' );
        $.library.modalClose();
        return false;
    })
        
    //Androidの場合はエディタが使えないため、ソース入力に切り替える
    //管理画面がスマフォ対応している場合のみ有効
    <?php
    if(method_exists($LACNE->library["admin_view"] , 'is_Android') && $LACNE->library["admin_view"]->is_Android()) :
    ?>
    if($('#edit-p').size() > 0)
    {
        //editor.setMode( 'source' ); //Andoridの場合、ckeditor側が自動で切り替えをしてくれるため、ここはコメントアウトする    
    }
    <?php
    endif;
    ?>
    
    //message
    <?php
    if((isset($err) && $err) || (isset($message) && $message)) {
    ?>
        $.library.do_slideDown_message("#comp_message");
    <?php
    }
    ?>
    
});

//詳細の表示 / 非表示切り替え
function disp_detail_area(val)
{
    if(val == 1){ 
    	$(".detail-edit").show(); 
    	$("#deteil_info_message").hide();
    }
    else{ 
    	$(".detail-edit").hide(); 
    	$("#deteil_info_message").show();
    }
}
// -----------------------------------------------
</script>
