<?php

    $page_setting = array(
        "title" => "エラー",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/complete.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "error" , array("cancel_page"=>$_cancel_page));
?>


<?php
$error_str = "エラーが発生しました。再度操作をおこなって下さい。";
if(isset($err) && $err) $error_str = $err;
?>

<section class="section">
<div class="section-inside">
<div class="alert error pie"><span class="icon">エラー</span><p class="fl"><?=$error_str?></p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="<?=$cancel_page?>"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<!-- .section-inside // --></div>
<!-- .section // --></section>



<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>