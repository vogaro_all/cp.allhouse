<?php

    $page_setting = array(
        "title" => "ソーシャル連携",
        "js" => array(LACNE_SHAREDATA_PATH."/js/prettyLoader/js/jquery.prettyLoader.js"),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/confirm.css", LACNE_SHAREDATA_PATH."/js/prettyLoader/css/prettyLoader.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "social");
?>


<section class="section">
<?php if(!isset($err) || !$err) : ?>
<h1 class="head-line01 smp">この<?=KEYWORD_KIJI?>をソーシャルメディアに投稿しますか？</h1>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">この<?=KEYWORD_KIJI?>をソーシャルメディアに投稿しますか？</p></div>
<?php else: ?>
<h1 class="head-line01 smp"><?=$err?></h1>
<div class="alert error pie pc"><span class="icon">情報</span><p class="fl"><?=$err?></p></div>
<?php endif; ?>
<form action="<?=$submit_link?>" method="post">
<div class="input">
<p class="social">
<?php
if(isset($connect_info["twitter"]["account"]) && $connect_info["twitter"]["account"]) :
?>
<input type="checkbox" name='twitter' value='1' <?=(!$_POST || isset($_POST["twitter"]))?"checked='checked'":""?> /><span class="twitter">Twitter</span>
<?php
endif;
if(isset($connect_info["facebook"]["account"]) && $connect_info["facebook"]["account"]) :
?>
<input type="checkbox" name='facebook' value='1' <?=(!$_POST || isset($_POST["facebook"]))?"checked='checked'":""?> /><span class="facebook">facebook</span>
<?php
endif;
?>
</p>
<p><textarea name="message"><?=(isset($message))?$message:""?></textarea></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" id="btn_sns_submit" name="send" value="投稿する" class="pie" /></p>
<p class="btn-type02 pie"><a href="<?=$complete_page?>"><span class="pie">投稿しない</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>



<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>