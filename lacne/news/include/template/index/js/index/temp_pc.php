<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.tooltip();
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $('figcaption a.tip').hover(function(){
            $('#Main .toolTip .text').text($(this).closest('figure').find('img').attr('alt'));
    },function(){
            $('#Main .toolTip .text').text('');
    });
    $('.list-type01 li').hover(function(){
            $(this).addClass('highlight');
    },function(){
            $(this).removeClass('highlight');
    });

});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    $('#Main p.name').text($('#Side p.name').text());
	
    $('.list-type01 li:last').addClass('last');
    $('.list-type01 li:nth-child(even)').addClass('odd');
    $('.list-type01 li:last').addClass('last');



});

// -----------------------------------------------
</script>
