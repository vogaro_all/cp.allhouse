<?php
use lacne\core\model\Post;
return function($request, $response, $service, $app){
    global $APP_DIRNAME;

    $app->lacne->load_library(array('login' , 'post')); //library load
    $login_id = $app->login_id;

    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のindex/以下にあるため）
    $app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/index");

    //最新の記事リスト表示数
    $output_num = $app->lacne->library["admin_view"]->get_num_per_page("index_index");
    $render_data = array(
        "login_id" => $login_id,
        "APP_DIRNAME" => $APP_DIRNAME,
        "data_list" => with(new Post())->get_list(1 , $output_num), //最近の記事リスト取得
    );

    //承認待ちデータ数をカウント（オプションが有効な場合のみ利用）
    if(method_exists(with(new Post()), "data_cnt_waiting"))
    {
        $render_data["cnt_wait"] = with(new Post())->data_cnt_waiting();
    }

    return $app->lacne->render("index" , $render_data, true);
};

