<?php namespace LACNE\CMS\NEWS;
use Klein\App;
use Klein\Klein;


require_once(dirname(__FILE__)."/../include/setup.php");
global $LACNE;

function _require_once( $path ){ return require_once( dirname(__FILE__) . $path ); };

$app = new App();
$app->register('lacne', function() use($LACNE){
    return $LACNE;
});

$app->register('login_id', function() use($LACNE){
    $LACNE->session->sessionStart();
    return $LACNE->library["login"]->IsSuccess();
});

$klein = new Klein(null, $app);

/**
 * トップページ
 */
$klein->with(ROUT_BASE_PATH, function () use ($klein) {
    $klein->respond('GET', '/index.php', _require_once('/index.php') );
});

/**
 * お知らせページ
 */
$klein->with(ROUT_BASE_PATH . '/article', function () use ($klein) {
    $prefix = '/article';
    $klein->respond(array('POST','GET'), '/index.php',          _require_once( $prefix . '/index.php') );   //トップページの表示
    $klein->respond(array('POST','GET'), '/edit.php',           _require_once( $prefix . '/edit.php') );    //作成・編集ページ
    $klein->respond(array('POST','GET'), '/publish.php',        _require_once( $prefix . '/publish.php') ); //公開・非公開
});

/**
 * カテゴリーページ
 */
$klein->with(ROUT_BASE_PATH . '/category', function () use ($klein) {
    $prefix = '/category';
    $klein->respond(array('POST','GET'), '/index.php',      _require_once( $prefix . '/index.php') );
    $klein->respond(array('POST','GET'), '/register.php',   _require_once( $prefix . '/register.php') );
});

/**
 * メディアページ
 */
$klein->with(ROUT_BASE_PATH . '/media', function () use ($klein) {
    $prefix = '/media';
    $klein->respond(array('POST','GET'), '/index.php',      _require_once( $prefix . '/index.php') );
    $klein->respond(array('POST','GET'), '/list.php',       _require_once( $prefix . '/list.php') );
});

$klein->onHttpError(function ($code, $router) {
    $router->response()->body(
        '[エラー]'.$code.':ルーティングに失敗しました！'
    );
});
//起動
$klein->dispatch();