<?php
$linkparam = "";
if($param){
	$linkparam .= "?".$param."&page=";
}else{
	$linkparam .= "?page=";
}


$around = 3;
if($page < 4 && 6 < $page_num)
{
	$start = 1;
	$end = 4;
	$isNextEllipsis = true;
}

if($page_num <= $page){
	$page = $page_num;
}


if($page >= 4 && $page_num - $around > $page && 6 < $page_num)
{
	$start = $page;
	$end = $page + 1;
	$isPrevEllipsis = $isNextEllipsis = true;
}

if($page_num - $around <= $page && 6 < $page_num)
{
	$start = $page;
	$end = $page + $around;
	$isPrevEllipsis = true;
}

if($page_num <= $end)
{
	$start = $page_num - $around;
	$end = $page_num;
}

if(6 >= $page_num)
{
	$start = 1;
	$end = $page_num;
}



$range = range($start, $end);
array_unshift($range, 1);
array_push($range, $page_num);
$range = array_unique($range, SORT_NUMERIC);
?>


<?php
//ページ数が2ページ以上の場合に表示
if($page_num>1) {
?>
<div class="pagination hidden__sp">
<?php
	if($page > 0) {
?>


	<?php if($page == 1) : ?>
		<div class="pagination__prev"><a>前へ</a></div>
	<?php  else : ?>
		<div class="pagination__prev"><a href="<?=$linkparam.($page-1)?>">前へ</a></div>
	<?php endif; ?>
	
	<?php foreach ($range as $index => $i) : ?>
		<?php if($isNextEllipsis && $i == $page_num): ?>
			<div class="pagination__dot">...</div>
		<?php endif; ?>

		<?php if( $page == $i ): ?>
				<div class="pagination__item--current"><span><?=$i?></span></div>
		<?php else: ?>
			<div class="pagination__item"><a href="<?=$linkparam.$i?>"><?=$i?></a></div>
		<?php endif; ?>

		<?php if($isPrevEllipsis && $i == 1): ?>
			<div class="pagination__dot">...</div>
		<?php endif; ?>
	<?php endforeach; ?>
	
	<?php if($page == $page_num) : ?>
		<div class="pagination__next"><a>次へ</a></div>
	<?php else : ?>
		<div class="pagination__next"><a href="<?=$linkparam.($page+1)?>">次へ</a></div>
	<?php endif; ?>


<?php
	}
?> 
<!-- .pager // --></div>
<?php
}
?>