<?php
$linkparam = "";
if($param){
    $linkparam .= "?".$param."&page=";
}else{
    $linkparam .= "?page=";
}


$around = 3;
if($page < 4 && 6 < $page_num)
{
    $start = 1;
    $end = 4;
    $isNextEllipsis = true;
}

if($page_num <= $page){
    $page = $page_num;
}


if($page >= 4 && $page_num - $around+1 > $page && 6 < $page_num)
{
    $start = $page-1;
    $end = $page + 1;
    $isPrevEllipsis = $isNextEllipsis = true;
}

if($page_num - $around+1 <= $page && 6 < $page_num)
{
    $start = $page;
    $end = $page + $around;
    $isPrevEllipsis = true;
}

if($page_num <= $end)
{
    $start = $page_num - $around;
    $end = $page_num;
}

if(6 >= $page_num)
{
    $start = 1;
    $end = $page_num;
}



$range = range($start, $end);
array_unshift($range, 1);
array_push($range, $page_num);
$range = array_unique($range, SORT_NUMERIC);
?>


<?php
//ページ数が2ページ以上の場合に表示
if($page_num>1) {
?>
            <div class="pagination">
                <ul class="clearfix">
<?php
	if($page > 0) {
?>

<?php 
if($page == 1) {
?>
<li><a class="prev">PREV</a></li>
<?php
} else {
?>
<li><a href="<?=$linkparam.($page-1)?>" class="prev">PREV</a></li>
<?php
}
?>
	<?php $roop_cnt = 1;?>
	<?php foreach ($range as $index => $i) :?>
		<?php if($isNextEllipsis && $i == $page_num): ?>
				<li>...</li>
		<?php endif; ?>

		<?php if( $page == $i ): ?>
                  <li <?php echo((isset($roop_cnt) && $roop_cnt == 4) ? 'class="pc"' : '' );?>><span><?=$i?></span></li>
		<?php else: ?>
                  <li <?php echo((isset($roop_cnt) && $roop_cnt == 4) ? 'class="pc"' : '' );?>><a href="<?=$linkparam.$i?>"><?=$i?></a></li>
		<?php endif; ?>

		<?php if($isPrevEllipsis && $i == 1): ?>
				<li>...</li>
		<?php endif; ?>
		<?php $roop_cnt++;?>
	<?php endforeach; ?>
	
<?php
	}
?> 

<?php
if($page == $page_num) {
?>
<li><a class="next">NEXT</a></li>
<?php
} else {
?>
<li><a href="<?=$linkparam.($page+1)?>" class="next">NEXT</a></li>
<?php
}
?>
                </ul>
            </div>
<?php
}
?>