<?php
$prev_page_txt = "Prev";
$next_page_txt = "Next";

$linkparam = "";
if($param){
        $linkparam .= "?".$param."&page=";
}else{
        $linkparam .= "?page=";
}


$first_disp_page = 2;//表示するページボタンの表示数 1,2,3・・15,16の場合は3個と数える
$second_disp_page = 1;//省略画像に続けるページボタンの表示数 1,2,3・・15,16の場合は2個と数える
$pointer_page=0;//ページ数が多い場合に切り替えする位置の指定
$omit_flg=true;//「・・・」表示フラグ

//全ページボタン表示する数
$all_disp_page=$first_disp_page+$second_disp_page;

$pointer=$page-$pointer_page;

if($pointer<=0) :
    $pointer=1;
endif;


$num_first_list=array();
$num_second_list=array();

//「・・・」を表示させる場合
if($page_num>$pointer+$all_disp_page) :
    //「・・・」の前半に表示するページ
    for($i=$pointer;$i<$first_disp_page+$pointer;$i++) :
        $num_first_list[]=$i;
    endfor;

    //「・・・」の後半に表示するページ
    for($i=$page_num-$second_disp_page+1;$i<=$page_num;$i++) :
        $num_second_list[]=$i;
    endfor;

    $omit_flg=true;


//「・・・」を表示させない場合
else :

    //ボタン表示数差分チェック用
    $num_check=$all_disp_page-($page_num-$pointer);

    if($num_check>0) :
        $first_num=$pointer-$num_check;

        if($first_num<1) :
            $first_num=1;
        endif;

        for($i=$first_num;$i<=$page_num;$i++) :
            $num_first_list[]=$i;
        endfor;

    else :

        for($i=$pointer;$i<=$page_num;$i++) :
            $num_first_list[]=$i;
        endfor;

    endif;
endif;

?>


<?php
//ページャのセンタリング表示用
$count=count($num_first_list)+count($num_second_list);
?>

<?php
//ページ数が2ページ以上の場合に表示
if($count>1) :
?>




<div class="pagination hidden__pc">
<?php
    if($page > 0) :

        if($page == 1) :
?>
<div class="pagination__prev"><a>前へ</a></div>
<?php
        else :
?>
<div class="pagination__prev"><a href="<?=$linkparam.($page-1)?>">前へ</a></div>

<?php
        endif;
?>

<?php
        foreach ($num_first_list as $i) :
            if($i == $page) :
?>
<div class="pagination__item--current"><span><?=$i?></span></div>
<?php
            else:
?>
<div class="pagination__item"><a href="<?=$linkparam.$i?>"><?=$i?></a></div>
<?php
            endif;
            $first_cnt++;
        endforeach;
?>

<?php
        if($omit_flg && count($num_second_list) != 0) :
?>
<div class="pagination__dot">...</div>
<?php
        endif;
?>

<?php
        foreach ($num_second_list as $i) :
            if($i == $page) :
?>
<div class="pagination__item--current"><span><?=$i?></span></div>
<?php
            else:
?>
<div class="pagination__item"><a href="<?=$linkparam.$i?>"><?=$i?></a></div>
<?php
            endif;
        endforeach;
?>
<?php
        if($page == $page_num) :
?>
<div class="pagination__next"><a>次へ</a></div>
<?php
        else :
?>

<div class="pagination__next"><a href="<?=$linkparam.($page+1)?>">次へ</a></div>
<?php
        endif;

    endif;
?>

<!-- .pager // --></div>


<?php
endif;
?>