<?php

/*--------------------------------------------------------
 *  ページテンプレート設定(PC / index)
 *
 *  TEMPLATE_TAG_S01 : INDEX一覧書き出しの最初に書き出すもの（空でも可）
 *  TEMPLATE_TAG_S02 : INDEX一覧書き出しの最後に書き出すもの（空でも可）
 *
 *  TEMPLATE_TAG_LOOP ； INDEX一覧書き出しでループさせるもの
 *  TEMPLATE_TAG_LOOP_TOP ； INDEX一覧書き出しでループさせるうち、一番最初に書き出す特殊なもの（空でも可）
 *  TEMPLATE_TAG_LOOP_LAST； INDEX一覧書き出しでループさせるうち、一番最後に書き出す特殊なもの（空でも可）
 *
 *  ※LOOP設定はそれぞれタグ中に [fieldname] を埋め込める
 *  　[title] : タイトルを書き出す
 *    [output_date] : 日付を書き出す（表示形式は TEMPLATE_FORMAT_DATE で指定）
 *    [category] : カテゴリID
 *    [category_name] : カテゴリ名
 *  　[newicon] : NEWアイコンを書き出す ($newIconItem で指定)
 *　　その他、postsテーブルのフィールド名を[]でくくるとその値が補完される
 *
 *  TEMPLATE_FORMAT_DATE : 日付の表示形式設定
 *  TEMPLATE_TAG_ERROR : 記事がなかった場合の表示設定
 *-------------------------------------------------------*/

$TEMPLATE_SETTING = array(
    
    //書き出しタグ
    "TEMPLATE_TAG_START" => "<ul id=\"tab-[tab-category]\" class=\"tab-content current clearfix\">" ,
    "TEMPLATE_TAG_END"  => "</ul>" ,
    "TEMPLATE_TAG_LOOP" => "<li><a[detail_linktag_param]><div class=\"news-img\"><img src=\"[thumb_image]\" class=\"pc\" alt=\"\"><img src=\"/assets/img/img_news_sp.png\" class=\"sp\" alt=\"\"><span class=\"news-category\">[category_name]</span></div><div class=\"news-intro\"><p class=\"news-ttl\">[title]</p><p class=\"news-date\">[output_date]</p></div></a></li>",
    "TEMPLATE_TAG_LOOP_TOP" => "",
    "TEMPLATE_TAG_LOOP_LAST" => "",
    
    //書き出し日付形式
    "TEMPLATE_FORMAT_DATE" => "Y/m/d",
    //書き出しエラー出力
    "TEMPLATE_TAG_ERROR" => "<li><p>新しい情報はありません。</p></li>" ,
    
    //NEWアイコンを出す場合、何日前までアイコンを出すか指定
    "TEMPLATE_NEWICON" => 7,
    //ページャ対応するか
    "TEMPLATE_PAGER" => false,
    //1ページ単位の表示数
    "TEMPLATE_PAGE_LIMIT" => "",
   
    //NEWアイコンを出す場合、アイコンNoを指定（デフォルトは1)
    "TEMPLATE_NEWICON_NO" => 1,
    //NEWアイコンの指定(imgタグでもCSSでも)
    "TEMPLATE_NEWICON_ITEM" => array(
        1 => "【NEW!】",
    )
);

/*--------------------------------------------------------
 * スマートフォン用設定（PC設定と異なる箇所のみ上書きするイメージ）
 *-------------------------------------------------------*/

$TEMPLATE_SETTING["smph"] = array(
    //"TEMPLATE_TAG_START" => "*INDEX_3(SMPH)_TEMPLATE<br /><div class=\"news-list\">" ,
    //"TEMPLATE_TAG_END"  => "</div>" ,
    //"TEMPLATE_TAG_LOOP" => "<p class=\"date\">[newicon]・<a[detail_linktag_param]>[output_date]</a>（[category_name]）</p><p class=\"title\">・[title]<span><img src=\"[_meta_img1]\" /></span></p>"
);

/*--------------------------------------------------------
 * ガラケー用設定（PC設定と異なる箇所のみ上書きするイメージ）
 *-------------------------------------------------------*/

$TEMPLATE_SETTING["mobi"] = array(
    //"TEMPLATE_TAG_S01" => "<dl class=\"news-list\">" ,
);

?>