<?php
$linkparam = "";
if($param){
    $linkparam .= "?".$param."&page=";
}else{
    $linkparam .= "?page=";
}

$display_no = 5;
$first_no = 1;
$pointer_page=2;//ページ数が多い場合に切り替えする位置の指定
$last_no = $display_no;

if ($page_num>=$display_no) {

    if ($page - $pointer_page <= 1) {
        $first_no = 1;
        $last_no = $display_no;
    } elseif ($page + $pointer_page <= $page_num) {
        $first_no = $page - $pointer_page;
        $last_no = $page + $pointer_page;
    } else {
        $first_no = $page_num - ($display_no-1);
        $last_no = $page_num;
    }
    
} else {
    $last_no = $page_num;
}
?>


<?php
if($page_num > 1) {
?>
    <div id="pagination">
<?php 
if($page == 1) {
?>
<a><span><img src="/assets/img/common/img_previous.png" alt="prev"></span></a>
<?php
} else {
?>
<a href="<?=$linkparam.($page-1)?>"><span><img src="/assets/img/common/img_previous.png" alt="prev"></span></a>
<?php
}
?>
      <ul>
<?php
// for($i=1;$i<=$page_num;$i++) {
for($i=$first_no;$i<=$last_no;$i++) {
    if($i == $page) {
?>
        <li class="page-item_<?php echo(sprintf('%02d', $i));?> active"><span><?php echo(sprintf('%02d', $i));?></span></li>
<?php
    } else {
?>
<!--
<a href="<?php echo($linkparam.$i);?>"></a>
-->
		<li class="page-item_<?php echo(sprintf('%02d', $i));?>"><span><?php echo(sprintf('%02d', $i));?></span></li>
                    
<?php
    }
}
?>
</ul>



<?php
if($page == $page_num) {
?>
<a><span><img src="/assets/img/common/img_next.png" alt="next"></span></a>
<?php
} else {
?>
<a href="<?=$linkparam.($page+1)?>"><span><img src="/assets/img/common/img_next.png" alt="next"></span></a>
<?php
}
?>
    </div>
<?php
}
?>

