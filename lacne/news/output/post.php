<?php

/**-------------------------------------------------------------
 *  
 * 記事データ表示用
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// -------------------------------------------------------------

include_once(dirname(__FILE__)."/../include/setup.php");
include_once(LACNE_SHAREDATA_DIR."/include/output/post.php");

/**
 * 表示関連の処理の追記はここで
 */
?>
