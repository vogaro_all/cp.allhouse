<?php
use lacne\core\model\Post;
use lacne\core\model\PostMeta;

/**-------------------------------------------------------------
 *  
 * 記事データ取得
 * ※各コンテンツ（お知らせ/news,イベント情報/event など）ごとのデータをまとめて取得する
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// -------------------------------------------------------------

require_once(dirname(__FILE__)."/../share/include/setup.php");
require_once(LACNE_SHAREDATA_DIR."/include/output/post.php");

$LACNE->load_library(array('post' , 'output', 'media'));

//News用のテーブルから記事データを5件取得する（テーブル名をnews_posts , news_postmeta , news_media , news_category としていた場合）
//setTablePrefix()でテーブルPrefixを"news"にセットしてデータ取得対象テーブルを切換え（news_posts や news_postmetaテーブルが対象になる)
with(new Post())->setTablePrefix('news');
$newsData = LACNE_PostList(array(
	"num" => 5,
	//"category" => 1,
	//"page_limit" => 10,
	//"date_target" => "2014-07%",
	//"postmeta" => true
));
//echo "News Data:<br>";
//var_dump($newsData);


//Event用のテーブルからイベントデータを5件取得する（テーブル名をevent_posts , event_postmeta , event_media , event_category としていた場合）
//setTablePrefix()でテーブルPrefixを"event"にセットしてデータ取得対象テーブルを切換え（event_posts や event_postmetaテーブルが対象になる)
with(new Post())->setTablePrefix('event');
$eventData = LACNE_PostList(array(
	"num" => 5
));
//echo "Event Data:<br>";
//var_dump($eventData);

?>
