<?php

/**
 *-------------------------------------------------------------------------
 *
 *  TEST
 *
 *-------------------------------------------------------------------------
 */

require_once(dirname(__FILE__)."/../news/output/post.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="javascript" />
<title>一覧ページサンプル</title>
</head>

<body>
    
<div id="list1">
<p>■日時指定とMETA拡張取得して表示</p>
<?php
$page_limit = 3;
$params = array(
        "num" 		=> "",
        "category" 	=> "",
        "page_limit" 	=> $page_limit,
        "date_target"   => "2012-04", //2012年04月のデータのみ
        "postmeta"      => 1
);

LACNE_PostList($params); //データを取得する

$data_number = getNumCurrentPage($page_limit); //データ件数まわりの情報を取得
echo "全".getListCnt()."件中".$data_number["num1"]." 〜 ".$data_number["num2"]."件を表示<br />";
echo renderList("list_sample"); //データを書き出し
?>
</div>

<?php
echo renderPager($page_limit); //pager 
?>


</body>
</html>
