<?php

/**
 *-------------------------------------------------------------------------
 *
 *  TEST
 *
 *-------------------------------------------------------------------------
 */

require_once(dirname(__FILE__)."/../news/output/post.php");

LACNE_Post(); //詳細データを取得しセット ($_GET["id"]をデータidとして関数内部で扱う。$_GET["id"]値以外をidとしている場合は、この関数に引数として渡す

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:mixi="http://mixi-platform.com/ns#"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="javascript" />
<title><?=the_title(); //タイtル?></title>
<meta name="description" content="<?=the_meta_description()?>" />
<meta name="keywords" content="<?=the_meta_keyword()?>" />
<link rel="stylesheet" type="text/css" href="sample_lacne_detail.css" media="all" />
<script type="text/javascript" src="../share/js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="../share/js/fancybox/jquery.fancybox-1.3.4.js"></script>
<link rel="stylesheet" href="../share/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript">
<?php
    //モーダルウィンドウ表示のためのjsコードを書き出し
    //本文中のimgタグをモーダル対応させる
    set_jscode_modallink();
?>
</script>
<?php
//OGP関連のタグを書き出し（Social_btnのOption有効の場合）
echo output_ogp();
?>
</head>

<body>

title : <?=the_title()?><br />
date : <?=the_date()?><br />
body : <?=the_body()?> <br />
meta_key : <?=  the_meta_keyword()?><br />
meta_dec : <?=  the_meta_description()?> <br />
category_id : <?=  the_category()?><br />
category_name : <?=  the_categoryname()?><br />
<?=pr(get_categories())?>

<br />
LPO : <?=lpo_content("lpo")?>
<br />


<br />
the_field : <?=the_field("meta_description")?>
<br />

<br />
META_img1 : <?=the_postmeta_field("img1")?>
<br />
<br />
<br />

<?=set_social_btn_twitter()?>
<?=set_social_btn_facebook()?>
<?=set_social_btn_mixi()?>

</body>
</html>
